TARGET := \
  C00 \
  C01 \
  C02 \
  C03 \
  C04 \
  C05

EXC00 :=                           \
  ex00/ft_putchar.c                \
  ex01/ft_print_alphabet.c         \
  ex02/ft_print_reverse_alphabet.c \
  ex03/ft_print_numbers.c          \
  ex04/ft_is_negative.c            \
  ex05/ft_print_comb.c             \
  ex06/ft_print_comb2.c            \
  ex07/ft_putnbr.c                 \
  ex08/ft_print_combn.c

EXC01 :=                           \
  ex00/ft_ft.c                     \
  ex01/ft_ultimate_ft.c            \
  ex02/ft_swap.c                   \
  ex03/ft_div_mod.c                \
  ex04/ft_ultimate_div_mod.c       \
  ex05/ft_putstr.c                 \
  ex06/ft_strlen.c                 \
  ex07/ft_rev_int_tab.c            \
  ex08/ft_sort_int_tab.c

EXC02 :=                           \
  ex00/ft_strcpy.c                 \
  ex01/ft_strncpy.c                \
  ex02/ft_str_is_alpha.c           \
  ex03/ft_str_is_numeric.c         \
  ex04/ft_str_is_lowercase.c       \
  ex05/ft_str_is_uppercase.c       \
  ex06/ft_str_is_printable.c       \
  ex07/ft_strupcase.c              \
  ex08/ft_strlowcase.c             \
  ex09/ft_strcapitalize.c          \
  ex10/ft_strlcpy.c                \
  ex11/ft_putstr_non_printable.c   \
  ex12/ft_print_memory.c

EXC03 :=                           \
  ex00/ft_strcmp.c                 \
  ex01/ft_strncmp.c                \
  ex02/ft_strcat.c                 \
  ex03/ft_strncat.c                \
  ex04/ft_strstr.c                 \
  ex05/ft_strlcat.c

EXC04 :=                           \
  ex00/ft_strlen.c                 \
  ex01/ft_putstr.c                 \
  ex02/ft_putnbr.c                 \
  ex03/ft_atoi.c                   \
  ex04/ft_putnbr_base.c            \
  ex05/ft_atoi_base.c

EXC05 :=                           \
  ex00/ft_iterative_factorial.c    \
  ex01/ft_recursive_factorial.c    \
  ex02/ft_iterative_power.c        \
  ex03/ft_recursive_power.c        \
  ex04/ft_fibonacci.c              \
  ex05/ft_sqrt.c                   \
  ex06/ft_is_prime.c               \
  ex07/ft_find_next_prime.c        \
  ex08/ft_ten_queens_puzzle.c

MKFILE_PATH := $(dir $(abspath $(lastword $(MAKEFILE_LIST))))

SRC :=         \
  $(PROJECT).c \
  core.c

SRC := $(addprefix $(MKFILE_PATH)/src/, $(SRC))

INCLUDE := \
	-I$(MKFILE_PATH)/include

FLAGS :=        \
  -ggdb         \
  -Wall         \
  -Wextra       \
  -Werror       \
  -Wshadow      \
  -nostartfiles \
  -fsanitize=address \
  -fsanitize=undefined


platform=$(shell uname)
ifeq ($(platform),Darwin)
  FLAGS += -Wl,-e,_wescande_main,-dead_strip
else
  FLAGS += -Wl,-e,wescande_main,--allow-multiple-definition 
endif


BIN ?= "$(MKFILE_PATH)$(PROJECT).wescande.correction"

PROJECT_FILES := $(addprefix $(PWD)/, $(EX$(PROJECT)))
FILES ?= $(PROJECT_FILES)
FILE_CHECK := $(foreach f, $(FILES), $(if $(wildcard $f),$f,))
EX_CHECK := $(strip $(foreach f, $(FILE_CHECK), $(shell echo "$f" | sed -e 's/^.*\/ex/ex/g' -e 's/\/.*$$//g')))
FILE_NO_CHECK := $(foreach f, $(FILES), $(if $(wildcard $f),,$f))
EX_NO_CHECK := $(strip $(foreach f, $(FILE_NO_CHECK), $(shell echo "$f" | sed -e 's/^.*\/ex/ex/g' -e 's/\/.*$$//g')))
# TODO print ex without correction even on a specific correction (d02 ex01 ex03)

DEFINE ?= $(addprefix -D, $(shell echo $(EX_CHECK) | tr '[a-z]' '[A-Z]'))

all:
	@echo -e "\033[33mUse wescande.correction.sh\033[0m"

$(TARGET):
	gcc $(USER_OPTIONS) $(DEFINE) $(SRC) $(FLAGS) $(INCLUDE) $(FILE_CHECK) -o $(BIN)

%:
	echo -e "\033[31mTarget available are [$(TARGET)]\033[0m"
	false

.PHONY: $(TARGET)
