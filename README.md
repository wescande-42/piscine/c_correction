# Correction piscine C 42
This tool can be use to check basic errors.
Norme is not check, cheat is not check.

## Quick installation:
```
sh -c "$(curl -fsSL https://gitlab.com/wescande-42/piscine/c_correction/-/raw/master/setup.sh)"
```
## Usage:
Man is accessible with:
```
wescande_correction -h
```

## Update:
The quick install command perform an update if it was already install.

Update alternative, run this command:
```
(cd $HOME/42_c_correction_wescande && git pull)
```

## Progress:
So good so far:
* C00
* C01
* C02
* C03
* C04
* C05
* C06
* C07

## Thanks
 - Jhalford
