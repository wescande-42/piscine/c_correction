#pragma once
#ifndef CORE_UTILS_H
#define CORE_UTILS_H

#include <inttypes.h>
#include <stdio.h>
#include <stdbool.h>
#include <ctype.h>
#include <string.h>
#include <stdlib.h>
#include <limits.h>
#include <unistd.h>


#define NO_COL  "\033[0m"
#define RED     "\033[31m"
#define GREEN   "\033[32m"
#define YELLOW  "\033[33m"
#define BLUE    "\033[34m"

#define UNDERLINE     "\033[4m"
#define EOF_UNDERLINE "\033[24m"

#define D_MSG(color, f, ...)      dprintf(2, color f NO_COL "\n", ##__VA_ARGS__)
#define MSG(color, f, ...)        dprintf(1, color f NO_COL "\n", ##__VA_ARGS__)
#define INTERN_ERR(f, ...)        MSG(YELLOW, __FILE__ ": " "(%d): " RED "INTERNAL ERROR - please report to maintainer: " f , __LINE__, ##__VA_ARGS__)

#define D_FAILED(f, ...)          D_MSG(RED, "  " f, ##__VA_ARGS__)
#define FAILED(f, ...)            MSG(RED,   "  " f, ##__VA_ARGS__)
#define SUCCESS(f, ...)           MSG(GREEN, "  " f, ##__VA_ARGS__)

#define SUC(f, ...)               MSG(GREEN,  f, ##__VA_ARGS__)
#define INFO(f, ...)              MSG(YELLOW, f, ##__VA_ARGS__)
#define ERR(f, ...)               MSG(RED,    f, ##__VA_ARGS__)
#define MISC(f, ...)              MSG(BLUE, f, ##__VA_ARGS__)

typedef struct test_handler_s test_handler_t;

struct test_handler_s {
  const char *name;
  const uint32_t timeout_ms;
  int (*const testfn)(int argc, const char *argv[], test_handler_t *test);
  bool (*const checkfn)(int argc, const char *argv[], const int fd, const int fd_error, test_handler_t *test);
};

typedef struct exercice_handler_s {
  const char *fn_name;
  test_handler_t **tests;
} exercice_handler_t;

extern int force_alloc_to_fail;

bool wescande_is_str_check(int (*fn)(char *s), char *ok_str, char err_str[][10]);
bool wescande_memcmp_fd_no_print(const int fd, const char *ref, ssize_t ref_len);
bool wescande_memcmp_fd(const int fd, const char *ref, ssize_t ref_len);
void wescande_output_num(int n);
void wescande_output_str(const char *s);
ssize_t wescande_catch(const int fd, char **s);
ssize_t wescande_check_empty_stdout(const int fd);
ssize_t wescande_check_empty_stderr(const int fd_err);
int wescande_convert_to_printable(const bool pad, char *dest, const size_t capacity, const char *src, const ssize_t len);

#define TOKENPASTE(x, y) x ## y
#define TOKENPASTE2(x, y) TOKENPASTE(x, y)

#define __CREATE_TEST_TIME(IDX,NAME,TIME,TESTFN,CHECKFN)                     \
  static int TOKENPASTE2(testfn_, IDX)(int argc, const char *argv[], test_handler_t *test)               \
  { (void)argc;(void)argv;(void)test;TESTFN return 0; }                      \
  static bool TOKENPASTE2(checkfn_, IDX)(int argc, const char *argv[], const int fd, const int fd_error, test_handler_t *test) \
  { (void)argc;(void)argv;(void)test; if (wescande_check_empty_stderr(fd_error)) { return false; } CHECKFN return !wescande_check_empty_stdout(fd); }  \
  static test_handler_t TOKENPASTE2(test_,IDX) = {                           \
    .name = NAME,                                                            \
    .timeout_ms = TIME,                                                      \
    .testfn = TOKENPASTE2(testfn_,IDX),                                      \
    .checkfn = TOKENPASTE2(checkfn_,IDX),                                    \
  }

#define CREATE_TEST_TIME(NAME,TIME,TESTFN,CHECKFN)                           \
  __CREATE_TEST_TIME(__COUNTER__,NAME,TIME,TESTFN,CHECKFN)
#define CREATE_TEST(NAME,TESTFN,CHECKFN)                                     \
  __CREATE_TEST_TIME(__COUNTER__,NAME,500,TESTFN,CHECKFN)

#define CREATE_EX(...)                                                       \
  static test_handler_t *test[] = { __VA_ARGS__ NULL };                      \
  exercice_handler_t ex = {                                                  \
    .fn_name = FN_NAME,                                                      \
    .tests = test,                                                           \
  };

#define MAX_PRINT_SIZE (2048 * 2 * 2 * 2)

#define EXPAND_TO_PRINTABLE(NAME,src,size,pad)                                                           \
  const char *NAME;                                                                                            \
  int NAME##_size; (void)NAME##_size;                                                                    \
  char NAME##_printable_##str[MAX_PRINT_SIZE + 1];                                                       \
  if (size > MAX_PRINT_SIZE) {                                                                           \
    NAME = src;                                                                                          \
  } else {                                                                                               \
    NAME##_size = wescande_convert_to_printable(pad, NAME##_printable_##str, MAX_PRINT_SIZE, src, size); \
    NAME = NAME##_printable_##str;                                                                       \
  }

#define TO_PRINTABLE(NAME,src,size) EXPAND_TO_PRINTABLE(NAME,src,size,false)
#define TO_PAD_PRINTABLE(NAME,src,size) EXPAND_TO_PRINTABLE(NAME,src,size,true)

#endif
