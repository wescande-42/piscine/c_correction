#!/bin/sh
set -e

if [ "$(echo -e | wc -c)" -lt 2 ]
then
  OPT_E="-e"
fi

raz="\033[0m"
red="\033[31m"
green="\033[32m"
yellow="\033[33m"

INSTALL_PATH=$HOME/42_c_correction_wescande

if [ -d $INSTALL_PATH ]; then
  echo $OPT_E $red"Already installed !"$raz
  echo $OPT_E $yellow"update in progress. . ."$raz
  (cd $INSTALL_PATH && git pull) || ( echo $OPT_E $red"Update has failed, you may want to delete $INSTALL_PATH with following command 'rm -rf $INSTALL_PATH' and relaunch this setup"$raz; exit 1 )
  echo $OPT_E $green"Update done"$raz
else
  echo $OPT_E $yellow"Cloning repository to $INSTALL_PATH"$raz
  git clone https://gitlab.com/wescande-42/piscine/c_correction $INSTALL_PATH
  echo $OPT_E $yellow"Add alias to current terminal. (other already opended terminal need to be restarted)"$raz
  alias wescande_correction='$INSTALL_PATH/wescande_correction.sh'
  echo $OPT_E $yellow"Adding alias to bashrc and zshrc"$raz
  echo "alias wescande_correction='$INSTALL_PATH/wescande_correction.sh'" | tee -a $HOME/.bashrc $HOME/.zshrc
fi
echo $OPT_E $green"Please read $INSTALL_PATH/README.md to understand basic usage"$raz
