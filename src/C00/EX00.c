#include "core_utils.h"

void ft_putchar(char c);

CREATE_TEST("print A", { ft_putchar('A'); }, { return wescande_memcmp_fd(fd, "A", 1); });
CREATE_TEST("print z", { ft_putchar('z'); }, { return wescande_memcmp_fd(fd, "z", 1); });
CREATE_TEST("print 8", { ft_putchar('8'); }, { return wescande_memcmp_fd(fd, "8", 1); });
CREATE_TEST("print 'SOH'", { ft_putchar(1); }, { return wescande_memcmp_fd(fd, "\x1", 1); });
CREATE_TEST("print 'DEL'", { ft_putchar(0x7F); }, { return wescande_memcmp_fd(fd, "\x7F", 1); });

CREATE_EX(
    &test_0,
    &test_1,
    &test_2,
    &test_3,
    &test_4,
    )

