#include "core_utils.h"

void ft_print_reverse_alphabet(void);

CREATE_TEST("", { ft_print_reverse_alphabet(); }, { return wescande_memcmp_fd(fd, "zyxwvutsrqponmlkjihgfedcba", 26); });

CREATE_EX(
    &test_0,
    )


