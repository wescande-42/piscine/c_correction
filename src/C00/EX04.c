#include "core_utils.h"

void ft_is_negative(int n);

CREATE_TEST("sign of 0", { ft_is_negative(0); }, { return wescande_memcmp_fd(fd, "P", 1); });
CREATE_TEST("sign of 1", { ft_is_negative(1); }, { return wescande_memcmp_fd(fd, "P", 1); });
CREATE_TEST("sign of '0' - 1", { ft_is_negative('0' - 1); }, { return wescande_memcmp_fd(fd, "P", 1); });
CREATE_TEST("sign of '0'", { ft_is_negative('0'); }, { return wescande_memcmp_fd(fd, "P", 1); });
CREATE_TEST("sign of INT_MAX", { ft_is_negative(INT_MAX); }, { return wescande_memcmp_fd(fd, "P", 1); });
CREATE_TEST("sign of -1", { ft_is_negative(-1); }, { return wescande_memcmp_fd(fd, "N", 1); });
CREATE_TEST("sign of INT_MIN", { ft_is_negative(INT_MIN); }, { return wescande_memcmp_fd(fd, "N", 1); });

CREATE_EX(
    &test_0,
    &test_1,
    &test_2,
    &test_3,
    &test_4,
    &test_5,
    &test_6,
    )

