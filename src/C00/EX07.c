#include "core_utils.h"
#include "c00_utils.h"

void ft_putnbr(int n);

CREATE_TEST("print 0", { ft_putnbr(0); }, { return wescande_memcmp_fd(fd, "0", 1); });
CREATE_TEST("print 460010", { ft_putnbr(460010); }, { return wescande_memcmp_fd(fd, "460010", 6); });
CREATE_TEST("print -238945", { ft_putnbr(-238945); }, { return wescande_memcmp_fd(fd, "-238945", 7); });
CREATE_TEST("print int max value", { ft_putnbr(INT_MAX); }, { return wescande_memcmp_fd(fd, "2147483647", 10); });
CREATE_TEST("print int min value", { ft_putnbr(INT_MIN); }, { return wescande_memcmp_fd(fd, "-2147483648", 11); });

CREATE_EX(
    &test_0,
    &test_1,
    &test_2,
    &test_3,
    &test_4,
    )

