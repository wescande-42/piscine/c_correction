#include "core_utils.h"
#include "c00_utils.h"

void ft_print_combn(int n);
CREATE_TEST("comb of 1", { ft_print_combn(1); }, { char s[] = STR_EX_08_1; return wescande_memcmp_fd(fd, s, sizeof(s) - 1); });
CREATE_TEST("comb of 2", { ft_print_combn(2); }, { char s[] = STR_EX_08_2; return wescande_memcmp_fd(fd, s, sizeof(s) - 1); });
CREATE_TEST("comb of 3", { ft_print_combn(3); }, { char s[] = STR_EX_08_3; return wescande_memcmp_fd(fd, s, sizeof(s) - 1); });
CREATE_TEST("comb of 4", { ft_print_combn(4); }, { char s[] = STR_EX_08_4; return wescande_memcmp_fd(fd, s, sizeof(s) - 1); });
CREATE_TEST("comb of 5", { ft_print_combn(5); }, { char s[] = STR_EX_08_5; return wescande_memcmp_fd(fd, s, sizeof(s) - 1); });
CREATE_TEST("comb of 6", { ft_print_combn(6); }, { char s[] = STR_EX_08_6; return wescande_memcmp_fd(fd, s, sizeof(s) - 1); });
CREATE_TEST("comb of 7", { ft_print_combn(7); }, { char s[] = STR_EX_08_7; return wescande_memcmp_fd(fd, s, sizeof(s) - 1); });
CREATE_TEST("comb of 8", { ft_print_combn(8); }, { char s[] = STR_EX_08_8; return wescande_memcmp_fd(fd, s, sizeof(s) - 1); });
CREATE_TEST("comb of 9", { ft_print_combn(9); }, { char s[] = STR_EX_08_9; return wescande_memcmp_fd(fd, s, sizeof(s) - 1); });

CREATE_EX(
    &test_0,
    &test_1,
    &test_2,
    &test_3,
    &test_4,
    &test_5,
    &test_6,
    &test_7,
    &test_8,
    )

