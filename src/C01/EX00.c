#include "core_utils.h"

void ft_ft(int *n);
CREATE_TEST("", {
    int i = 15;
    ft_ft(&i);
    if (i != 42)
      D_FAILED("Got %d instead of %d", i, 42);
    }, {});

CREATE_EX(
    &test_0,
    )
