#include "core_utils.h"

void ft_ultimate_ft(int *********nbr);

CREATE_TEST("", {
    int i = 5;
    int *n1 = &i;
    int **n2 = &n1;
    int ***n3 = &n2;
    int ****n4 = &n3;
    int *****n5 = &n4;
    int ******n6 = &n5;
    int *******n7 = &n6;
    int ********n8 = &n7;
    ft_ultimate_ft(&n8);
    if (i != 42)
      D_FAILED("Got %d instead of %d", i, 42);
    }, {});

CREATE_EX(
    &test_0,
    )


