#include "core_utils.h"

void ft_swap(int *a, int *b);

CREATE_TEST("", {
    int a = 5;
    int b = 10;
    ft_swap(&a, &b);
    if (b != 5 || a != 10)
      D_FAILED("Got a=%d and b=%d instead of a=%d and b=%d", a, b, 10, 5);
    }, {});

CREATE_EX(
    &test_0,
    )

