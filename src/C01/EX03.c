#include "core_utils.h"

void ft_div_mod(int a, int b, int *div, int *mod);

CREATE_TEST("", {
    int a = 2147864;
    int b = 8975;
    int div;
    int mod;
    ft_div_mod(a, b, &div, &mod);
    if (div != a / b || mod != a % b)
      D_FAILED("With a=%d, b=%d Got div=%d and mod=%d instead of div=%d and mod=%d", a, b, div, mod, a/b, a%b);
    }, {});

CREATE_EX(
    &test_0,
    )

