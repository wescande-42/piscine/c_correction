#include "core_utils.h"

void ft_ultimate_div_mod(int *a, int *b);

CREATE_TEST("", {
    int a = 7897897;
    int b = 35664;
    const int aO = a;
    const int bO = b;
    ft_ultimate_div_mod(&a, &b);
    if (a != aO / bO || b != aO % bO)
      D_FAILED("With a=%d, b=%d Got a(div)=%d and b(mod)=%d instead of div=%d and mod=%d", aO, bO, a, b, aO/bO, aO%bO);
    }, {});

CREATE_EX(
    &test_0,
    )


