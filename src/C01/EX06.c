#include "core_utils.h"
#include "c01_utils.h"

int ft_strlen(char *str);

CREATE_TEST("strlen Bonjour", {
    char *s = "Bonjour";
    const int len = ft_strlen(s);
    const int ref_len = strlen(s);
    if (len != ref_len)
      D_FAILED("Got %d instead of %d", len, ref_len);
    }, {});
CREATE_TEST("big strlen man page of git", {
    char *s = STR_EX_06;
    const int len = ft_strlen(s);
    const int ref_len = strlen(s);
    if (len != ref_len)
      D_FAILED("Got %d instead of %d", len, ref_len);
    }, {});

CREATE_EX(
    &test_0,
    &test_1,
    )

