#include "core_utils.h"

void ft_rev_int_tab(int *tab, int size);

CREATE_TEST("rev a nullptr tab with no size", { ft_rev_int_tab(NULL, 0); }, {});
CREATE_TEST("rev a tab with a odd size", {
    int size = 511;
    int tab[size];
    int tabref[size];
    for (int i = 0; i < size; ++i) {
      tab[i] = rand();
    }
    memcpy(tabref, tab, sizeof(int) * size);
    ft_rev_int_tab(tab, size);
    for (int i = 0; i < size; ++i) {
      if (tab[i] != tabref[size - i - 1]) {
        D_FAILED("At i=%d. Got %d instead of %d", i, tab[i], tabref[size - i - 1]);
        return 0;
      }
    }
    }, {});
CREATE_TEST("rev a tab with a even size", {
    int size = 2048;
    int tab[size];
    int tabref[size];
    for (int i = 0; i < size; ++i) {
      tab[i] = rand();
    }
    memcpy(tabref, tab, sizeof(int) * size);
    ft_rev_int_tab(tab, size);
    for (int i = 0; i < size; ++i) {
      if (tab[i] != tabref[size - i - 1]) {
        D_FAILED("At i=%d. Got %d instead of %d", i, tab[i], tabref[size - i - 1]);
        return 0;
      }
    }
    }, {});

CREATE_EX(
    &test_0,
    &test_1,
    &test_2,
    )


