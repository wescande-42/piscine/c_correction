#include "core_utils.h"

void ft_sort_int_tab(int *tab, int size);

CREATE_TEST("sort a nullptr tab with no size", { ft_sort_int_tab(NULL, 0); }, {});
CREATE_TEST("sort a tab with a odd size", {
    int size = 511;
    int tab[size];
    int tabref[size];
    for (int i = 0; i < size; ++i) {
      tab[i] = rand();
    }
    memcpy(tabref, tab, sizeof(int) * size);
    ft_sort_int_tab(tab, size);
    for (int i = 0; i < size - 1; ++i) {
      if (tab[i] > tab[i + 1]) {
        D_FAILED("Wrong order 'tab[%d]=%d' > 'tab[%d]=%d'", i, tab[i], i + 1, tab[i+1]);
        return 0;
      }
    }
    }, {});
CREATE_TEST("sort a tab with a huge even size", {
    int size = 2048;
    int tab[size];
    int tabref[size];
    for (int i = 0; i < size; ++i) {
      tab[i] = rand();
    }
    memcpy(tabref, tab, sizeof(int) * size);
    ft_sort_int_tab(tab, size);
    for (int i = 0; i < size - 1; ++i) {
      if (tab[i] > tab[i + 1]) {
        D_FAILED("Wrong order 'tab[%d]=%d' > 'tab[%d]=%d'", i, tab[i], i + 1, tab[i+1]);
        return 0;
      }
    }
    }, {});

CREATE_EX(
    &test_0,
    &test_1,
    &test_2,
    )

