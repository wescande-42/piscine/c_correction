#include "core_utils.h"

char *ft_strcpy(char *dest, char *src);

CREATE_TEST("", {
    const char ref[] = "Je suis une chaine de char";
    char str[sizeof(ref)];
    char dest[sizeof(ref)];
    memcpy(str, ref, sizeof(ref));
    char *ret = ft_strcpy(dest, str);
    if (ret != dest) {
      D_FAILED("Wrong return value.");
    }
    if (strcmp(dest, str)) {
      D_FAILED("Wrong copy:\n" RED "    Your:     [%s]\n" GREEN "    Correct:  [%s]", dest, ref);
    }
    if (strcmp(ref, str)) {
      D_FAILED("You must not change src");
    }
    }, { });

CREATE_EX(
    &test_0,
    )

