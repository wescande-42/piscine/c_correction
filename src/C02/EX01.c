#include "core_utils.h"

char *ft_strncpy(char *dest, char *src, unsigned int n);

CREATE_TEST("len is bigger than src", {
    const char ref[] = "Je suis une chaine\0 de char";
    char str[sizeof(ref)];
    char refdest[]  = "bbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbb";
    char dest[sizeof(refdest)];

    memcpy(str, ref, sizeof(ref));
    memcpy(dest, refdest, sizeof(refdest));
    strncpy(refdest, ref, sizeof(ref) - 3);
    char *ret = ft_strncpy(dest, str, sizeof(ref) - 3);
    if (ret != dest) {
      D_FAILED("Wrong return value.");
    }
    if (memcmp(dest, refdest, sizeof(refdest))) {
      TO_PRINTABLE(d, dest, sizeof(ref) - 3)
      TO_PRINTABLE(r, refdest, sizeof(ref) - 3)
      D_FAILED("Wrong ncopy:\n" RED "    Your:     [%s]\n" GREEN "    Correct:  [%s]", d, r);
    }
    if (memcmp(ref, str, sizeof(ref))) {
      D_FAILED("You must not change src");
    }
    }, {});
CREATE_TEST("len is smaller than src", {
    const char ref[] = "Je suis une chaine\0 de char";
    char str[sizeof(ref)];
    char refdest[]  = "bbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbb";
    char dest[sizeof(refdest)];
    memcpy(str, ref, sizeof(ref));
    memcpy(dest, refdest, sizeof(refdest));
    strncpy(refdest, ref, 5);
    char *ret = ft_strncpy(dest, str, 5);
    if (ret != dest) {
      D_FAILED("Wrong return value.");
    }
    if (memcmp(dest, refdest, sizeof(refdest))) {
      TO_PRINTABLE(d, dest, 5)
      TO_PRINTABLE(r, refdest, 5)
      D_FAILED("Wrong ncopy:\n" RED "    Your:     [%s]\n" GREEN "    Correct:  [%s]", d, r);
    }
    if (memcmp(ref, str, sizeof(ref))) {
      D_FAILED("You must not change src");
    }
    }, {});

CREATE_EX(
    &test_0,
    &test_1,
    )

