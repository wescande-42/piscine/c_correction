#include "core_utils.h"

int ft_str_is_alpha(char *dest);

CREATE_TEST("full alpha & ALPHA", ({
    char *str = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ";
    wescande_output_num(ft_str_is_alpha(str));
    });, { return wescande_memcmp_fd(fd, "1", 1); });
CREATE_TEST("not alpha 'a' - 1", ({
    char str[] = {'a', 'z', 'a' - 1, 'A', 'Z', 0};
    wescande_output_num(ft_str_is_alpha(str));
    });, { return wescande_memcmp_fd(fd, "0", 1); });
CREATE_TEST("not alpha '0'", ({
    char str[] = {'a', 'z', '0', 'A', 'Z', 0};
    wescande_output_num(ft_str_is_alpha(str));
    });, { return wescande_memcmp_fd(fd, "0", 1); });
CREATE_TEST("not alpha 'z' + 1", ({
    char str[] = {'a', 'z', 'z' + 1, 'A', 'Z', 0};
    wescande_output_num(ft_str_is_alpha(str));
    });, { return wescande_memcmp_fd(fd, "0", 1); });
CREATE_TEST("not alpha 'A' - 1", ({
    char str[] = {'a', 'z', 'A' - 1, 'A', 'Z', 0};
    wescande_output_num(ft_str_is_alpha(str));
    });, { return wescande_memcmp_fd(fd, "0", 1); });
CREATE_TEST("not alpha 'Z' + 1", ({
    char str[] = {'a', 'z', 'Z' + 1, 'A', 'Z', 0};
    wescande_output_num(ft_str_is_alpha(str));
    });, { return wescande_memcmp_fd(fd, "0", 1); });

CREATE_EX(
    &test_0,
    &test_1,
    &test_2,
    &test_3,
    &test_4,
    &test_5,
    )


