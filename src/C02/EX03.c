#include "core_utils.h"

int ft_str_is_numeric(char *dest);

CREATE_TEST("all digits", ({
    char *str = "0123456789";
    wescande_output_num(ft_str_is_numeric(str));
    });, { return wescande_memcmp_fd(fd, "1", 1); });
CREATE_TEST("not num 9", ({
    char str[] = {9, 0};
    wescande_output_num(ft_str_is_numeric(str));
    });, { return wescande_memcmp_fd(fd, "0", 1); });
CREATE_TEST("not num 'a'", ({
    char str[] = {'a', 0};
    wescande_output_num(ft_str_is_numeric(str));
    });, { return wescande_memcmp_fd(fd, "0", 1); });
CREATE_TEST("not num 'z'", ({
    char str[] = {'z', 0};
    wescande_output_num(ft_str_is_numeric(str));
    });, { return wescande_memcmp_fd(fd, "0", 1); });
CREATE_TEST("not num 'A'", ({
    char str[] = {'A', 0};
    wescande_output_num(ft_str_is_numeric(str));
    });, { return wescande_memcmp_fd(fd, "0", 1); });
CREATE_TEST("not num 'Z'", ({
    char str[] = {'Z', 0};
    wescande_output_num(ft_str_is_numeric(str));
    });, { return wescande_memcmp_fd(fd, "0", 1); });
CREATE_TEST("not num '0' - 1", ({
    char str[] = {'0' - 1, 0};
    wescande_output_num(ft_str_is_numeric(str));
    });, { return wescande_memcmp_fd(fd, "0", 1); });
CREATE_TEST("not num '9' + 1", ({
    char str[] = {'9' + 1, 0};
    wescande_output_num(ft_str_is_numeric(str));
    });, { return wescande_memcmp_fd(fd, "0", 1); });

CREATE_EX(
    &test_0,
    &test_1,
    &test_2,
    &test_3,
    &test_4,
    &test_5,
    &test_6,
    &test_7,
    )


