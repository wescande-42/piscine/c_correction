#include "core_utils.h"

int ft_str_is_lowercase(char *dest);

CREATE_TEST("full alphabet", ({
    char *str = "abcdefghijklmnopqrstuvwxyz";
    wescande_output_num(ft_str_is_lowercase(str));
    });, { return wescande_memcmp_fd(fd, "1", 1); });
CREATE_TEST("some maj", ({
    char *str = "AZ";
    wescande_output_num(ft_str_is_lowercase(str));
    });, { return wescande_memcmp_fd(fd, "0", 1); });
CREATE_TEST("'a' - 1", ({
    char str[] = {'a' - 1, 0};
    wescande_output_num(ft_str_is_lowercase(str));
    });, { return wescande_memcmp_fd(fd, "0", 1); });
CREATE_TEST("'z' + 1", ({
    char str[] = {'z' + 1, 0};
    wescande_output_num(ft_str_is_lowercase(str));
    });, { return wescande_memcmp_fd(fd, "0", 1); });

CREATE_EX(
    &test_0,
    &test_1,
    &test_2,
    &test_3,
    )


