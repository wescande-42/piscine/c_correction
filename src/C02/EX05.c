#include "core_utils.h"

int ft_str_is_uppercase(char *dest);

CREATE_TEST("full ALPHABET", ({
    char *str = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
    wescande_output_num(ft_str_is_uppercase(str));
    });, { return wescande_memcmp_fd(fd, "1", 1); });
CREATE_TEST("some min", ({
    char *str = "az";
    wescande_output_num(ft_str_is_uppercase(str));
    });, { return wescande_memcmp_fd(fd, "0", 1); });
CREATE_TEST("'A' - 1", ({
    char str[] = {'A' - 1, 0};
    wescande_output_num(ft_str_is_uppercase(str));
    });, { return wescande_memcmp_fd(fd, "0", 1); });
CREATE_TEST("'Z' + 1", ({
    char str[] = {'Z' + 1, 0};
    wescande_output_num(ft_str_is_uppercase(str));
    });, { return wescande_memcmp_fd(fd, "0", 1); });

CREATE_EX(
    &test_0,
    &test_1,
    &test_2,
    &test_3,
    )


