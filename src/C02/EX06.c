#include "core_utils.h"

int ft_str_is_printable(char *dest);

CREATE_TEST("full ascii printable range", ({
    char *str = " @!$^*&%(fsdvbbwq ()[]:; ~";
    wescande_output_num(ft_str_is_printable(str));
    });, { return wescande_memcmp_fd(fd, "1", 1); });
CREATE_TEST("' ' - 1", ({
    char str[] = {' ' - 1, 0};
    wescande_output_num(ft_str_is_printable(str));
    });, { return wescande_memcmp_fd(fd, "0", 1); });
CREATE_TEST("'~' + 1", ({
    char str[] = {'~' + 1, 0};
    wescande_output_num(ft_str_is_printable(str));
    });, { return wescande_memcmp_fd(fd, "0", 1); });

CREATE_EX(
    &test_0,
    &test_1,
    &test_2,
    )


