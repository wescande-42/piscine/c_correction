#include "core_utils.h"

char *ft_strupcase(char *str);

CREATE_TEST("", ({
    char ref[] = {'~' + 1, 'a', 'z', 'a' - 1, 'z' + 1, 'A', '0', 0};
    char str[sizeof(ref)];
    memcpy(str, ref, sizeof(ref));
    char *dest = ft_strupcase(str);
    if (dest != str) {
      D_FAILED("Wrong return value.");
    }
    for (size_t i = 0; i < sizeof(ref); ++i) {
      ref[i] = toupper(ref[i]);
    }
    if (strcmp(ref, str)) {
      TO_PRINTABLE(d, dest, sizeof(ref) - 1)
      TO_PRINTABLE(r, ref, sizeof(ref) - 1)
      D_FAILED("Wrong conversion to uppercase:\n" RED "    Your:     [%s]\n" GREEN "    Correct:  [%s]", d, r);
    }
    });, {});

CREATE_EX(
    &test_0,
    )

