#include "core_utils.h"

char *ft_strlowcase(char *str);

CREATE_TEST("", ({
    char ref[] = {'~' + 1, 'A', 'Z', 'A' - 1, 'Z' + 1, 'a', '0', 0};
    char str[sizeof(ref)];
    memcpy(str, ref, sizeof(ref));
    char *dest = ft_strlowcase(str);
    if (dest != str) {
      D_FAILED("Wrong return value.");
    }
    for (size_t i = 0; i < sizeof(ref); ++i) {
      ref[i] = tolower(ref[i]);
    }
    if (strcmp(ref, str)) {
      TO_PRINTABLE(d, dest, sizeof(ref) - 1)
      TO_PRINTABLE(r, ref, sizeof(ref) - 1)
      D_FAILED("Wrong conversion to lowercase:\n" RED "    Your:     [%s]\n" GREEN "    Correct:  [%s]", d, r);
    }
    });, {});

CREATE_EX(
    &test_0,
    )


