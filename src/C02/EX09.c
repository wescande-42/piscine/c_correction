#include "core_utils.h"

char *ft_strcapitalize(char *str);

void __capitalize(char *s) {
  while(*s) {
    while(*s && !isalnum(*s)) {
      ++s;
    }
    if (!*s) { return ; }
    *s = toupper(*s);
    ++s;
    while(isalnum(*s)) {
      *s = tolower(*s);
      ++s;
    }
  }
}

CREATE_TEST("Pdf example", ({
    char ref[] = "Hi, How are you ? 42word You are a good-looking people; fifty+and+one";
    char str[sizeof(ref)];
    memcpy(str, ref, sizeof(ref));
    char *dest = ft_strcapitalize(str);
    if (dest != str) {
      D_FAILED("Wrong return value.");
    }
    __capitalize(ref);
    if (strcmp(ref, str)) {
      TO_PRINTABLE(d, dest, sizeof(ref) - 1)
      TO_PRINTABLE(r, ref, sizeof(ref) - 1)
      D_FAILED("Wrong conversion:\n" RED "    Your:     [%s]\n" GREEN "    Correct:  [%s]", d, r);
    }
    });, {});
CREATE_TEST("More delimiter", ({
    char ref[] = " st[art_with#space7Hi,@How are you@!%#^&HeY ? 42wor~d You are a g\x1food-loo\x7fking pe{ople; f`ifty+and+one bigwOOOO\x01oord                   ";
    char str[sizeof(ref)];
    memcpy(str, ref, sizeof(ref));
    char *dest = ft_strcapitalize(str);
    if (dest != str) {
      D_FAILED("Wrong return value.");
    }
    __capitalize(ref);
    if (strcmp(ref, str)) {
      TO_PRINTABLE(d, dest, sizeof(ref) - 1)
      TO_PRINTABLE(r, ref, sizeof(ref) - 1)
      D_FAILED("Wrong conversion:\n" RED "    Your:     [%s]\n" GREEN "    Correct:  [%s]", d, r);
    }
    });, {});

CREATE_EX(
    &test_0,
    &test_1,
    )


