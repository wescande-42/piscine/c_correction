#include "core_utils.h"

unsigned int ft_strlcpy(char *dest, char *src, unsigned int size);

size_t wescande_strlcpy(char *dst, const char *src, size_t size)
{ const size_t srclen = strlen(src);if(size) {--size;const int cpylen = (srclen > size) ?size:srclen;memcpy(dst, src, cpylen);dst[cpylen] = '\0';} return (srclen);}

CREATE_TEST("len is bigger than src", {
    const char ref[] = "Je suis une chaine\0 de char";
    char str[sizeof(ref)];
    char refdest[]  = "bbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbb";
    char dest[sizeof(refdest)];

    const int reflen = strlen(ref);
    memcpy(str, ref, sizeof(ref));
    memcpy(dest, refdest, sizeof(refdest));
    wescande_strlcpy(refdest, ref, sizeof(ref) - 3);
    int ret = ft_strlcpy(dest, str, sizeof(ref) - 3);
    if (ret != reflen) {
      D_FAILED("Wrong return value.");
    }
    if (memcmp(dest, refdest, sizeof(refdest))) {
      TO_PRINTABLE(d, dest, sizeof(dest))
      TO_PRINTABLE(r, refdest, sizeof(dest))
      D_FAILED("Wrong ncopy:\n" RED "    Your:     [%s]\n" GREEN "    Correct:  [%s]", d, r);
    }
    if (memcmp(ref, str, sizeof(ref))) {
      D_FAILED("You must not change src");
    }
    }, {});
CREATE_TEST("len is smaller than src", {
    const char ref[] = "Je suis une chaine\0 de char";
    char str[sizeof(ref)];
    char refdest[]  = "bbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbb";
    char dest[sizeof(refdest)];

    const int reflen = strlen(ref);
    memcpy(str, ref, sizeof(ref));
    memcpy(dest, refdest, sizeof(refdest));
    wescande_strlcpy(refdest, ref, 5);
    int ret = ft_strlcpy(dest, str, 5);
    if (ret != reflen) {
      D_FAILED("Wrong return value.");
    }
    if (memcmp(dest, refdest, sizeof(refdest))) {
      TO_PRINTABLE(d, dest, sizeof(dest))
      TO_PRINTABLE(r, refdest, sizeof(dest))
      D_FAILED("Wrong ncopy:\n" RED "    Your:     [%s]\n" GREEN "    Correct:  [%s]", d, r);
    }
    if (memcmp(ref, str, sizeof(ref))) {
      D_FAILED("You must not change src");
    }
    }, {});
CREATE_TEST("with a size = 0", {
    const char ref[] = "Je suis une chaine\0 de char";
    char str[sizeof(ref)];
    char refdest[]  = "bbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbb";
    char dest[sizeof(refdest)];

    const int reflen = strlen(ref);
    memcpy(str, ref, sizeof(ref));
    memcpy(dest, refdest, sizeof(refdest));
    wescande_strlcpy(refdest, ref, 0);
    int ret = ft_strlcpy(dest, str, 0);
    if (ret != reflen) {
      D_FAILED("Wrong return value.");
    }
    if (memcmp(dest, refdest, sizeof(refdest))) {
      TO_PRINTABLE(d, dest, sizeof(dest))
      TO_PRINTABLE(r, refdest, sizeof(dest))
      D_FAILED("Wrong ncopy:\n" RED "    Your:     [%s]\n" GREEN "    Correct:  [%s]", d, r);
    }
    if (memcmp(ref, str, sizeof(ref))) {
      D_FAILED("You must not change src");
    }
    }, {});

CREATE_EX(
    &test_0,
    &test_1,
    &test_2,
    )


