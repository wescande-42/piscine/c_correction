#include "core_utils.h"

void ft_putstr_non_printable(char *str);

CREATE_TEST("All ascii range", {
    char ref[256];
    for (unsigned char c = 0; c < 255; ++c) {
      ref[c] = c + 1;
    }
    ref[255] = 0;
    char str[sizeof(ref)];
    memcpy(str, ref, sizeof(ref));
    ft_putstr_non_printable(str);
    if (memcmp(ref, str, sizeof(ref))) {
      D_FAILED("You must not change src");
    }
    }, {
      char ref[256];
      for (unsigned char c = 0; c < 255; ++c) {
        ref[c] = c + 1;
      }
      ref[255] = 0;
      TO_PAD_PRINTABLE(cmp, ref, sizeof(ref) - 1);
      return wescande_memcmp_fd(fd, cmp, cmp_size);
    });

CREATE_EX(
    &test_0,
    )


