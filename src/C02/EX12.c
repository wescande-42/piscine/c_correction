#include "core_utils.h"

void *ft_print_memory(void *addr, unsigned int size);

static char __12_0_mem[300];
const ssize_t mem_len = sizeof(__12_0_mem);

CREATE_TEST("", {
    for (int i = 0; i < mem_len; ++i) {
      __12_0_mem[i] = (char)i;
    }
    ft_print_memory(__12_0_mem, mem_len);
    for (int i = 0; i < mem_len; ++i) {
      if (__12_0_mem[i] != (char)i) {
        D_FAILED("You must not change src");
      }
    }
    }, {
      const char *base = "0123456789abcdef";
      bool ret = true;
      for (int i = 0; i < mem_len; ++i) {
        __12_0_mem[i] = (char)i;
      }
      char *s;
      const ssize_t len = wescande_catch(fd, &s);
      const ssize_t ref_len = (1 + mem_len / 16) * (16 + 1 + (16*2 + 8) + 1 + 16 + 1) - (16 - (mem_len % 16));
      const char *fd_str = s;
      char *line_start = s;
      uint8_t line = 0;
      unsigned char *ref_mem = (unsigned char *)__12_0_mem;
      const uint32_t addr_print_size = 16;
      while (s - fd_str < len && s - fd_str < ref_len) {
        line_start = s;
        if (s - fd_str + 16 > len) {
            FAILED("Cannot parsed addr: Not enough data");
            ret = false;
            break;
        }
        // Check address
        const char *s_addr = s;
        const uint64_t addr = strtoull(s_addr, &s, 16);
        const uint32_t addr_length = s - s_addr;
        if (addr_length != addr_print_size) {
          FAILED("Only %u digit found. Addr length should be display with %u digit", addr_length, addr_print_size);
          ret = false;
          break ;
        }
        if (addr != (uint64_t)ref_mem) {
          FAILED("Address get %p. Should avec been %p", (void *)addr, ref_mem);
          ret = false;
          break;
        }
        if (*s++ != ':') {
          FAILED("':' is missing");
          ret = false;
          break;
        }
        if (s - fd_str + (16 * 2 + 8) > len) {
            FAILED("Cannot parsed hexa memory: Not enough data");
            ret = false;
            break;
        }
        int i = 0;
        while (i < 16) {
          if (!(i % 2)) {
            if (*s++ != ' ') {
              FAILED("Delimiter ' ' is missing");
              ret = false;
              break;
            }
          }
          if ((uint64_t)ref_mem + i >= (uint64_t)__12_0_mem + mem_len) {
            if (*s++ != ' ' || *s++ != ' ') {
              FAILED("padding ' ' is missing");
              ret = false;
              break;
            }
          } else {
            if (*s++ != base[ref_mem[i] / 16] || *s++ != base[ref_mem[i] % 16]) {
              FAILED("Expect '%c' but got '%c'", base[ref_mem[i] / 16], *(s - 1));
              ret = false;
              break;
            }
          }
          ++i;
        }
        if (!ret) { break; }
        if (*s++ != ' ') {
          FAILED("Delimiter ' ' is missing");
          ret = false;
          break;
        }
        if (s - fd_str + (1 + (int64_t)__12_0_mem + mem_len - (int64_t)ref_mem) > len) {
            FAILED("Cannot parsed ascii memory: Not enough data");
            ret = false;
            break;
        }
        i = 0;
        while (i < 16 && (uint64_t)ref_mem + i < (uint64_t)__12_0_mem + mem_len) {
          if ((isprint(ref_mem[i]) && ref_mem[i] != *s) || (!isprint(ref_mem[i]) && *s != '.')) {
            FAILED("Wrong value:\n" RED "  Your>     '%c'\n" GREEN "  Expected> '%c'", *s, isprint(ref_mem[i]) ? ref_mem[i] : '.');
            ++s;
            ret = false;
            break ;
          }
          ++s;
          ++i;
        }
        if (!ret) { break; }
        if (*s++ != '\n') {
          FAILED("Delimiter '\\n' is missing");
          ret = false;
          break;
        }
        ref_mem += 16;
        ++line;
      }
      if (len != ref_len) {
        FAILED("Wrong length output\n" RED "    Your:     [%ld]\n" GREEN "    Correct:  [%ld]", len, ref_len);
        if (ret) s++;
        ret = false;
      }
      if (!ret) {
        FAILED("Error occured. %d/%zu char parsed. error at l%d:c%d:", (int)(s - fd_str), len, line, (int)(s - line_start));
        write(1, fd_str, s - fd_str - 1);
        write(1, RED UNDERLINE, sizeof(RED) + sizeof(UNDERLINE));
        TO_PRINTABLE(r_tmp,  fd_str + (s - fd_str - 1), 1);
        write(1, r_tmp, strlen(r_tmp));
        write(1, NO_COL EOF_UNDERLINE, sizeof(NO_COL) + sizeof(EOF_UNDERLINE));
        write(1, fd_str + (s - fd_str - 1) + 1,len-( s - fd_str - 1));
        write(1, "\n", 1);
        TO_PRINTABLE(r, fd_str, len);
        INFO("  Same data with non printable char displayed:\n" YELLOW "  {\n%s\n" YELLOW "  }", r);
        FAILED();
        FAILED("FYI: Format expected is:\n"
"xxxxxxxxxxxxxxxx: xxxx xxxx xxxx xxxx xxxx xxxx xxxx xxxx ................\n"
"xxxxxxxxxxxxxxxx: xxxx xxxx xxxx xxxx xxxx xx             ...........\n");
      }
      return ret;

    });

CREATE_EX(
    &test_0,
    )

