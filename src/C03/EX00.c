#include "core_utils.h"

int ft_strcmp(char *s1, char *s2);

static int WESCANDE_strcmp(const char *s1, const char *s2) {
  for ( ; *s1 == *s2; s1++, s2++)
    if (*s1 == '\0')
      return 0;
   return (*(unsigned char *)s1 - *(unsigned char *)s2);
   // This is what it should be return ((*(unsigned char *)s1 < *(unsigned char *)s2) ? -1 : +1);
}

CREATE_TEST("s1 > s2", {
    char *s1 = "Je suis une chaine de char";
    char *s2 = "Je suis";
    const int OLDref = WESCANDE_strcmp(s1, s2);
    const int ref = strcmp(s1, s2);
    const int user = ft_strcmp(s1, s2);
    if (OLDref != user && ref != user) {
      D_FAILED("Wrong cmp of [%s] & [%s]:\n" RED "    Your:     [%d]\n" GREEN "    Correct:  moulinette expect: [%d]  | your system return [%d]", s1, s2, user, OLDref, ref);
    }
    }, { });
CREATE_TEST("s1 < s2", {
    char *s1 = "Je suis";
    char *s2 = "Je suis une chaine de char";
    const int OLDref = WESCANDE_strcmp(s1, s2);
    const int ref = strcmp(s1, s2);
    const int user = ft_strcmp(s1, s2);
    if (OLDref != user && ref != user) {
      D_FAILED("Wrong cmp of [%s] & [%s]:\n" RED "    Your:     [%d]\n" GREEN "    Correct:  moulinette expect: [%d]  | your system return [%d]", s1, s2, user, OLDref, ref);
    }
    }, { });
CREATE_TEST("s1 = s2", {
    char *s1 = "Je suis une chaine de char";
    char *s2 = s1;
    const int OLDref = WESCANDE_strcmp(s1, s2);
    const int ref = strcmp(s1, s2);
    const int user = ft_strcmp(s1, s2);
    if (OLDref != user) {
      D_FAILED("Wrong cmp of [%s] & [%s]:\n" RED "    Your:     [%d]\n" GREEN "    Correct:  moulinette expect: [%d]  | your system return [%d]", s1, s2, user, OLDref, ref);
    }
    }, { });
CREATE_TEST("unsigned test", ({
    char s1[] = {'a', 'z', -50, 'a', 'b', 0};
    char s2[] = {'a', 'z', 25, 'a', 'b', 0};
    const int OLDref = WESCANDE_strcmp(s1, s2);
    const int ref = strcmp(s1, s2);
    const int user = ft_strcmp(s1, s2);
    if (OLDref != user) {
      TO_PRINTABLE(str1, s1, strlen(s1));
      TO_PRINTABLE(str2, s2, strlen(s2));
      D_FAILED("Wrong cmp of [%s] & [%s]:\n" RED "    Your:     [%d]\n" GREEN "    Correct:  moulinette expect: [%d]  | your system return [%d]", str1, str2, user, OLDref, ref);
    }
    });, { });

CREATE_EX(
    &test_0,
    &test_1,
    &test_2,
    &test_3,
    )
