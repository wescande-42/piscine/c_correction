#include "core_utils.h"

int ft_strncmp(char *s1, char *s2, unsigned int n);

static int WESCANDE_strncmp(const char *s1, const char *s2, size_t n)
{
  if (n == 0) return (0);
  do {
    if (*s1 != *s2)
      return (*(unsigned char *)s1 - *(unsigned char *)s2);
    if (*s1 == 0)
      break;
    ++s1;
    ++s2;
  } while (--n != 0);
  return (0);
}

static void do_compare(char *s1, char* s2, size_t len) {
    const int OLDref = WESCANDE_strncmp(s1, s2, len);
    const int ref = strncmp(s1, s2, len);
    const int user = ft_strncmp(s1, s2, len);
    if (OLDref != user) {
      TO_PRINTABLE(str1, s1, strlen(s1));
      TO_PRINTABLE(str2, s2, strlen(s2));
      D_FAILED("Wrong cmp of [%s] & [%s] len %zu:\n" RED "    Your:     [%d]\n" GREEN "    Correct:  moulinette expect: [%d]  | your system return [%d]", str1, str2, len, user, OLDref, ref);
    }
}

CREATE_TEST("s1 > s2", {
    char *s1 = "Je suis une chaine de char";
    char *s2 = "Je suis";
    const size_t len = strlen(s1);
    do_compare(s1, s2, len);
    }, { });
CREATE_TEST("0 len", {
    char *s1 = "Je suis une chaine de char";
    char *s2 = "Je suis";
    const size_t len = 0;
    do_compare(s1, s2, len);
    }, { });
CREATE_TEST("s1 < s2", {
    char *s1 = "Je suis";
    char *s2 = "Je suis une chaine de char";
    const size_t len = strlen(s1) + 2;
    do_compare(s1, s2, len);
    }, { });
CREATE_TEST("early match", {
    char *s1 = "Je suis une chaine de char";
    char *s2 = "Je suis";
    const size_t len = 3;
    do_compare(s1, s2, len);
    }, { });
CREATE_TEST("edge match", {
    char *s1 = "Je suis une chaine de char";
    char *s2 = "Je suis";
    const size_t len = 7;
    do_compare(s1, s2, len);
    }, { });
CREATE_TEST("unsigned test", ({
    char s1[] = {'a', 'z', -50, 'a', 'b', 0};
    char s2[] = {'a', 'z', 25, 'a', 'b', 0};
    const size_t len = strlen(s2);
    do_compare(s1, s2, len);
    });, { });

CREATE_EX(
    &test_0,
    &test_1,
    &test_2,
    &test_3,
    &test_4,
    &test_5,
    )


