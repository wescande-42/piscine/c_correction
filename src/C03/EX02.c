#include "core_utils.h"

char  *ft_strcat(char *dest, char *src);

CREATE_TEST("append to empty", {
    char *s0 = "";
    char *s1 = "Je ";
    char ref[20];
    char dest[20];
    memset(ref, 'A', 20);
    strcpy(ref, s0);
    memcpy(dest, ref, 20);
    strcat(ref, s1);
    char *user = ft_strcat(dest, s1);
    if (dest != user) {
      D_FAILED("Wrong return value");
    }
    if (memcmp(ref, dest, 20)) {
      TO_PRINTABLE(d, dest, 20)
      TO_PRINTABLE(r, ref, 20)
      D_FAILED("Wrong strcat(\"%s\", \"%s\"):\n" RED "    Your:     [%s]\n" GREEN "    Correct:  [%s]", s0, s1, d, r);
    }
    }, { });
CREATE_TEST("append empty", {
    char *s0 = "Je suis une ";
    char *s1 = "";
    char ref[30];
    char dest[30];
    memset(ref, 'A', 30);
    strcpy(ref, s0);
    memcpy(dest, ref, 30);
    strcat(ref, s1);
    char * user = ft_strcat(dest, s1);
    if (dest != user) {
      D_FAILED("Wrong return value");
    }
    if (memcmp(ref, dest, 30)) {
      TO_PRINTABLE(d, dest, 30)
      TO_PRINTABLE(r, ref, 30)
      D_FAILED("Wrong strlcat(\"%s\", \"%s\"):\n" RED "    Your:     [%s]\n" GREEN "    Correct:  [%s]", s0, s1, d, r);
    }
    }, { });
CREATE_TEST("append to existing", {
    char *s0 = "Je suis une ";
    char *s1 = "chaine de char.";
    char ref[30];
    char dest[30];
    memset(ref, 'A', 30);
    strcpy(ref, s0);
    memcpy(dest, ref, 30);
    strcat(ref, s1);
    char * user = ft_strcat(dest, s1);
    if (dest != user) {
      D_FAILED("Wrong return value");
    }
    if (memcmp(ref, dest, 30)) {
      TO_PRINTABLE(d, dest, 30)
      TO_PRINTABLE(r, ref, 30)
      D_FAILED("Wrong strlcat(\"%s\", \"%s\"):\n" RED "    Your:     [%s]\n" GREEN "    Correct:  [%s]", s0, s1, d, r);
    }
    }, { });

CREATE_EX(
    &test_0,
    &test_1,
    &test_2,
    )


