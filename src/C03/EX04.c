#include "core_utils.h"

char  *ft_strstr(char *str, char *tofind);

void test_strstr(char *base, char *tofind) {
    char *ref = strstr(base, tofind);
    char *user = ft_strstr(base, tofind);
    if (ref != user) {
      D_FAILED("Search [%s] in [%s]\n" RED "    Your:     [%s]\n" GREEN "    Correct:  [%s]", tofind, base, user, ref);
    }
}

CREATE_TEST("simple", {
    char *base = "ABCDEFGH";
    char *tofind = "B";
    test_strstr(base, tofind);
    }, { });
CREATE_TEST("a bit longer", {
    char *base = "ABCDEFGHABCDEFGH";
    char *tofind = "BCD";
    test_strstr(base, tofind);
    }, { });
CREATE_TEST("nested simple", {
    char *base = "AAB";
    char *tofind = "AB";
    test_strstr(base, tofind);
    }, { });
CREATE_TEST("nested", {
    char *base = "ABABABC";
    char *tofind = "ABABC";
    test_strstr(base, tofind);
    }, { });
CREATE_TEST(" No result can be found", {
    char *base = "ABABAB";
    char *tofind = "ABABC";
    test_strstr(base, tofind);
    }, { });
CREATE_TEST("Try with a empty string to found", {
    char *base = "ABABAB";
    char *tofind = "";
    test_strstr(base, tofind);
    }, { });
CREATE_TEST("Try with a empty string in str", {
    char *base = "";
    char *tofind = "ABABAB";
    test_strstr(base, tofind);
    }, { });
CREATE_TEST("Two empty string", {
    char *base = "";
    char *tofind = "";
    test_strstr(base, tofind);
    }, { });


CREATE_EX(
    &test_0,
    &test_1,
    &test_2,
    &test_3,
    &test_4,
    &test_5,
    &test_6,
    &test_7,
    )


