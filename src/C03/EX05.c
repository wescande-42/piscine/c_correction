#include "core_utils.h"

unsigned int  ft_strlcat(char *dest, char *src, unsigned int n);

static size_t wescande_strlcat(char * restrict dst, const char * restrict src, size_t maxlen) {
    const size_t srclen = strlen(src);
    const size_t dstlen = strnlen(dst, maxlen);
    if (dstlen == maxlen) return maxlen+srclen;
    if (srclen < maxlen-dstlen) {
        memcpy(dst+dstlen, src, srclen+1);
    } else {
        memcpy(dst+dstlen, src, maxlen-dstlen-1);
        dst[maxlen-1] = '\0';
    }
    return dstlen + srclen;
}

CREATE_TEST("length match", {
    char *s0 = "";
    char *s1 = "Je ";
    int lencat = 3;
    char ref[20];
    char dest[20];
    memset(ref, 'A', 20);
    strcpy(ref, s0);
    memcpy(dest, ref, 20);
    int cmp = wescande_strlcat(ref, s1, lencat);
    int user = ft_strlcat(dest, s1, lencat);
    if (cmp != user) {
      D_FAILED("Wrong return value");
    }
    if (memcmp(ref, dest, 20)) {
      TO_PRINTABLE(d, dest, 20)
      TO_PRINTABLE(r, ref, 20)
      D_FAILED("Wrong strlcat(\"%s\", \"%s\", %d):\n" RED "    Your:     [%s]\n" GREEN "    Correct:  [%s]", s0, s1,lencat, d, r);
    }
    }, { });
CREATE_TEST("length match is bigger", {
    char *s0 = "";
    char *s1 = "Je ";
    int lencat = 10;
    char ref[20];
    char dest[20];
    memset(ref, 'A', 20);
    strcpy(ref, s0);
    memcpy(dest, ref, 20);
    int cmp = wescande_strlcat(ref, s1, lencat);
    int user = ft_strlcat(dest, s1, lencat);
    if (cmp != user) {
      D_FAILED("Wrong return value");
    }
    if (memcmp(ref, dest, 20)) {
      TO_PRINTABLE(d, dest, 20)
      TO_PRINTABLE(r, ref, 20)
      D_FAILED("Wrong strlcat(\"%s\", \"%s\", %d):\n" RED "    Your:     [%s]\n" GREEN "    Correct:  [%s]", s0, s1,lencat, d, r);
    }
    }, { });
CREATE_TEST("length match is smaller", {
    char *s0 = "";
    char *s1 = "Je ";
    int lencat = 2;
    char ref[20];
    char dest[20];
    memset(ref, 'A', 20);
    strcpy(ref, s0);
    memcpy(dest, ref, 20);
    int cmp = wescande_strlcat(ref, s1, lencat);
    int user = ft_strlcat(dest, s1, lencat);
    if (cmp != user) {
      D_FAILED("Wrong return value");
    }
    if (memcmp(ref, dest, 20)) {
      TO_PRINTABLE(d, dest, 20)
      TO_PRINTABLE(r, ref, 20)
      D_FAILED("Wrong strlcat(\"%s\", \"%s\", %d):\n" RED "    Your:     [%s]\n" GREEN "    Correct:  [%s]", s0, s1,lencat, d, r);
    }
    }, { });
CREATE_TEST("real cat size == len of dest", {
    char *s0 = "Je suis une ";
    char *s1 = "chaine de char.";
    int lencat = 19;
    char ref[20];
    char dest[20];
    memset(ref, 'A', 20);
    strcpy(ref, s0);
    memcpy(dest, ref, 20);
    int cmp = wescande_strlcat(ref, s1, lencat);
    int user = ft_strlcat(dest, s1, lencat);
    if (cmp != user) {
      D_FAILED("Wrong return value");
    }
    if (memcmp(ref, dest, 20)) {
      TO_PRINTABLE(d, dest, 20)
      TO_PRINTABLE(r, ref, 20)
      D_FAILED("Wrong strlcat(\"%s\", \"%s\", %d):\n" RED "    Your:     [%s]\n" GREEN "    Correct:  [%s]", s0, s1,lencat, d, r);
    }
    }, { });
CREATE_TEST("test overflow protection on dest", {
    char *s1 = "de char.";
    int lencat = 2;
    char ref[20];
    char dest[20];
    memset(ref, 'A', 20);
    memcpy(dest, ref, 20);
    int cmp = wescande_strlcat(ref, s1, lencat);
    int user = ft_strlcat(dest, s1, lencat);
    if (cmp != user) {
      D_FAILED("Wrong return value");
    }
    if (memcmp(ref, dest, 20)) {
      TO_PRINTABLE(d, dest, 20)
      TO_PRINTABLE(r, ref, 20)
      D_FAILED("Wrong strlcat(\"%s\" no '\\0', \"%s\", %d):\n" RED "    Your:     [%s]\n" GREEN "    Correct:  [%s]", r, s1,lencat, d, r);
    }
    }, { });

CREATE_EX(
    &test_0,
    &test_1,
    &test_2,
    &test_3,
    &test_4,
    )
