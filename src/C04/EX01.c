#include "core_utils.h"
#include "c01_utils.h"

void ft_putstr(char *str);

CREATE_TEST("print Bonjour", { ft_putstr("bonjour"); }, { return wescande_memcmp_fd(fd, "bonjour", 7); });
CREATE_TEST("big print man page of git", { ft_putstr(STR_EX_05); }, { char s[] = STR_EX_05; return wescande_memcmp_fd(fd, s, sizeof(s) - 1); });

CREATE_EX(
    &test_0,
    &test_1,
    )


