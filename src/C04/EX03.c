#include "core_utils.h"

int ft_atoi(char *s);

static int wescande_atoi(char *s) {
  while(isspace(*s)) {++s;}
  int sign = 1;
  while (*s == '-' || *s == '+') {
    if (*s++ == '-') {
      sign *= -1;
    }
  }
  long n = 0;
  while(isdigit(*s)) {
    if (!n) {
      n = *s - '0';
    } else {
      n *= 10;
      n += *s - '0';
    }
    ++s;
  }
  return n * sign;
}

CREATE_TEST("0", {
    char *s = "0";
    int ref = wescande_atoi(s);
    int n = ft_atoi(s);
    if (n != ref) {
      D_FAILED("Wrong with input [%s]:\n" RED "    Your:     [%d]\n" GREEN "    Correct:  [%d]", s, n, ref);
    }
    }, {});
CREATE_TEST("+++++--460010", {
    char *s = "++++++--460010";
    int ref = wescande_atoi(s);
    int n = ft_atoi(s);
    if (n != ref) {
      D_FAILED("Wrong with input [%s]:\n" RED "    Your:     [%d]\n" GREEN "    Correct:  [%d]", s, n, ref);
    }
    }, {});
CREATE_TEST("-238945", {
    char *s = "-238945";
    int ref = wescande_atoi(s);
    int n = ft_atoi(s);
    if (n != ref) {
      D_FAILED("Wrong with input [%s]:\n" RED "    Your:     [%d]\n" GREEN "    Correct:  [%d]", s, n, ref);
    }
    }, {});
CREATE_TEST("int max value", {
    char *s = "2147483647";
    int ref = wescande_atoi(s);
    int n = ft_atoi(s);
    if (n != ref) {
      D_FAILED("Wrong with input [%s]:\n" RED "    Your:     [%d]\n" GREEN "    Correct:  [%d]", s, n, ref);
    }
    }, {});
CREATE_TEST("int min value", {
    char *s = "-2147483648";
    int ref = INT_MIN;
    int n = ft_atoi(s);
    if (n != ref) {
      D_FAILED("Wrong with input [%s]:\n" RED "    Your:     [%d]\n" GREEN "    Correct:  [%d]", s, n, ref);
    }
    }, {});
CREATE_TEST("space before int min value", {
    char *s = "    -2147483648";
    int ref = wescande_atoi(s);
    int n = ft_atoi(s);
    if (n != ref) {
      TO_PRINTABLE(r, s, strlen(s));
      D_FAILED("Wrong with input [%s]:\n" RED "    Your:     [%d]\n" GREEN "    Correct:  [%d]", r, n, ref);
    }
    }, {});
CREATE_TEST("space before int min value", {
    char *s = " \t\f \n  \r \v -2147483648";
    int ref = wescande_atoi(s);
    int n = ft_atoi(s);
    if (n != ref) {
      TO_PRINTABLE(r, s, strlen(s));
      D_FAILED("Wrong with input [%s]:\n" RED "    Your:     [%d]\n" GREEN "    Correct:  [%d]", r, n, ref);
    }
    }, {});
CREATE_TEST("some 0 space before int min value", {
    char *s = " \t\f \n  \r \v --000000214+-a7483648";
    int ref = wescande_atoi(s);
    int n = ft_atoi(s);
    if (n != ref) {
      TO_PRINTABLE(r, s, strlen(s));
      D_FAILED("Wrong with input [%s]:\n" RED "    Your:     [%d]\n" GREEN "    Correct:  [%d]", r, n, ref);
    }
    }, {});
CREATE_TEST("-0", {
    char *s = "-0";
    int ref = wescande_atoi(s);
    int n = ft_atoi(s);
    if (n != ref) {
      D_FAILED("Wrong with input [%s]:\n" RED "    Your:     [%d]\n" GREEN "    Correct:  [%d]", s, n, ref);
    }
    }, {});

CREATE_EX(
    &test_0,
    &test_1,
    &test_2,
    &test_3,
    &test_4,
    &test_5,
    &test_6,
    &test_7,
    &test_8,
    )

