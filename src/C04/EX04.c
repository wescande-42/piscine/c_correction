#include "core_utils.h"

void ft_putnbr_base(int n, char *base);

CREATE_TEST("base empty", { ft_putnbr_base(42, ""); }, {});
CREATE_TEST("base size 1", { ft_putnbr_base(42, "0"); }, {});
CREATE_TEST("base same char", { ft_putnbr_base(42, "01234526789"); }, {});
CREATE_TEST("base with '+' char", { ft_putnbr_base(42, "0123+456789"); }, {});
CREATE_TEST("base with '-' char", { ft_putnbr_base(42, "0123456789-"); }, {});
CREATE_TEST("print 0", { ft_putnbr_base(0, "01"); }, { return wescande_memcmp_fd(fd, "0", 1); });
CREATE_TEST("simple binary print", { ft_putnbr_base(42, "01"); }, { return wescande_memcmp_fd(fd, "101010", 6); });
CREATE_TEST("simple negative binary print", { ft_putnbr_base(-42, "01"); }, { return wescande_memcmp_fd(fd, "-101010", 7); });
CREATE_TEST("int max in base hexa", { ft_putnbr_base(INT_MAX, "0123456789ABCDEF"); }, { return wescande_memcmp_fd(fd, "7FFFFFFF", 8); });
CREATE_TEST("int min in base octal", { ft_putnbr_base(INT_MIN, "01234567"); }, { return wescande_memcmp_fd(fd, "-20000000000", 12); });

CREATE_EX(
    &test_0,
    &test_1,
    &test_2,
    &test_3,
    &test_4,
    &test_5,
    &test_6,
    &test_7,
    &test_8,
    &test_9,
    )

