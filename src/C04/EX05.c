#include "core_utils.h"

int ft_atoi_base(char *s, char *base);

CREATE_TEST("base empty", { if (ft_atoi_base("42", "")) { D_FAILED("Base error. Must return 0");} }, {});
CREATE_TEST("base size 1", { if (ft_atoi_base("42", "0")) { D_FAILED("Base error. Must return 0");} }, {});
CREATE_TEST("base same char", { if (ft_atoi_base("42", "01234526789")) { D_FAILED("Base error. Must return 0");} }, {});
CREATE_TEST("base with '+' char", { if (ft_atoi_base("42", "01234+56789")) { D_FAILED("Base error. Must return 0");} }, {});
CREATE_TEST("base with '-' char", { if (ft_atoi_base("42", "0123456789-")) { D_FAILED("Base error. Must return 0");} }, {});
CREATE_TEST("0 in base 01", {
    char *nbr = "0";
    char *base = "01";
    int ref = 0;
    int n = ft_atoi_base(nbr, base);
    if (n != ref) {
      D_FAILED("Wrong with ft_atoi_base(\"%s\", \"%s\"):\n" RED "    Your:     [%d]\n" GREEN "    Correct:  [%d]", nbr, base, n, ref);
    }
    }, {});
CREATE_TEST("42 in base 01", {
    char *nbr = "42";
    char *base = "01";
    int ref = 0;
    int n = ft_atoi_base(nbr, base);
    if (n != ref) {
      D_FAILED("Wrong with ft_atoi_base(\"%s\", \"%s\"):\n" RED "    Your:     [%d]\n" GREEN "    Correct:  [%d]", nbr, base, n, ref);
    }
    }, {});
CREATE_TEST("101010 in base 01", {
    char *nbr = "101010";
    char *base = "01";
    int ref = 42;
    int n = ft_atoi_base(nbr, base);
    if (n != ref) {
      D_FAILED("Wrong with ft_atoi_base(\"%s\", \"%s\"):\n" RED "    Your:     [%d]\n" GREEN "    Correct:  [%d]", nbr, base, n, ref);
    }
    }, {});
CREATE_TEST("-101010 in base 01", {
    char *nbr = "-101010";
    char *base = "01";
    int ref = -42;
    int n = ft_atoi_base(nbr, base);
    if (n != ref) {
      D_FAILED("Wrong with ft_atoi_base(\"%s\", \"%s\"):\n" RED "    Your:     [%d]\n" GREEN "    Correct:  [%d]", nbr, base, n, ref);
    }
    }, {});
CREATE_TEST("7FFFFFFF in hexa", {
    char *nbr = "7FFFFFFF";
    char *base = "0123456789ABCDEF";
    int ref = INT_MAX;
    int n = ft_atoi_base(nbr, base);
    if (n != ref) {
      D_FAILED("Wrong with ft_atoi_base(\"%s\", \"%s\"):\n" RED "    Your:     [%d]\n" GREEN "    Correct:  [%d]", nbr, base, n, ref);
    }
    }, {});
CREATE_TEST("-20000000000 in octal", {
    char *nbr = "   ---000020000000000";
    char *base = "01234567";
    int ref = INT_MIN;
    int n = ft_atoi_base(nbr, base);
    if (n != ref) {
      D_FAILED("Wrong with ft_atoi_base(\"%s\", \"%s\"):\n" RED "    Your:     [%d]\n" GREEN "    Correct:  [%d]", nbr, base, n, ref);
    }
    }, {});
CREATE_TEST("weird in weird base", {
    char *nbr = "   \t \f \n -+-+-+--+++utio8)(";
    char *base = "ut8io()";
    int ref = -25527;
    int n = ft_atoi_base(nbr, base);
    if (n != ref) {
      TO_PRINTABLE(s_n, nbr, strlen(nbr));
      TO_PRINTABLE(b, base, strlen(base));
      D_FAILED("Wrong with ft_atoi_base(\"%s\", \"%s\"):\n" RED "    Your:     [%d]\n" GREEN "    Correct:  [%d]", s_n, b, n, ref);
    }
    }, {});

CREATE_EX(
    &test_0,
    &test_1,
    &test_2,
    &test_3,
    &test_4,
    &test_5,
    &test_6,
    &test_7,
    &test_8,
    &test_9,
    &test_10,
    &test_11,
    )


