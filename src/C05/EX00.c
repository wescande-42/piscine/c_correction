#include "core_utils.h"

int ft_iterative_factorial(int nb);

CREATE_TEST("Info", {}, { MISC("    " FN_NAME " MUST NOT CALL " FN_NAME );});
CREATE_TEST("0", {
    int nb = 0;
    int ans = 1;
    int ret = ft_iterative_factorial(nb);
    if (ret != ans) {
      D_FAILED("Wrong with input [%d]:\n" RED "    Your:     [%d]\n" GREEN "    Correct:  [%d]", nb, ret, ans);
    }
    }, {});
CREATE_TEST("1", {
    int nb = 1;
    int ans = 1;
    int ret = ft_iterative_factorial(nb);
    if (ret != ans) {
      D_FAILED("Wrong with input [%d]:\n" RED "    Your:     [%d]\n" GREEN "    Correct:  [%d]", nb, ret, ans);
    }
    }, {});
CREATE_TEST("5", {
    int nb = 5;
    int ans = 120;
    int ret = ft_iterative_factorial(nb);
    if (ret != ans) {
      D_FAILED("Wrong with input [%d]:\n" RED "    Your:     [%d]\n" GREEN "    Correct:  [%d]", nb, ret, ans);
    }
    }, {});
CREATE_TEST("-5", {
    int nb = -5;
    int ans = 0;
    int ret = ft_iterative_factorial(nb);
    if (ret != ans) {
      D_FAILED("Wrong with input [%d]:\n" RED "    Your:     [%d]\n" GREEN "    Correct:  [%d]", nb, ret, ans);
    }
    }, {});
CREATE_TEST("12", {
    int nb = 12;
    int ans = 479001600;
    int ret = ft_iterative_factorial(nb);
    if (ret != ans) {
      D_FAILED("Wrong with input [%d]:\n" RED "    Your:     [%d]\n" GREEN "    Correct:  [%d]", nb, ret, ans);
    }
    }, {});

CREATE_EX(
    &test_0,
    &test_1,
    &test_2,
    &test_3,
    &test_4,
    &test_5,
    )

