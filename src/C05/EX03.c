#include "core_utils.h"

int ft_recursive_power(int nb, int power);

CREATE_TEST("Info", {}, { MISC("    " FN_NAME " MUST CALL " FN_NAME ); MISC("    Is it tail recursive ?"); });
CREATE_TEST("0^0", {
    int nb = 0;
    int power = 0;
    int ans = 1;
    int ret = ft_recursive_power(nb, power);
    if (ret != ans) {
      D_FAILED("Wrong with input [%d^%d]:\n" RED "    Your:     [%d]\n" GREEN "    Correct:  [%d]", nb, power, ret, ans);
    }
    }, {});
CREATE_TEST("1^0", {
    int nb = 1;
    int power = 0;
    int ans = 1;
    int ret = ft_recursive_power(nb, power);
    if (ret != ans) {
      D_FAILED("Wrong with input [%d^%d]:\n" RED "    Your:     [%d]\n" GREEN "    Correct:  [%d]", nb, power, ret, ans);
    }
    }, {});
CREATE_TEST("0^5", {
    int nb = 0;
    int power = 5;
    int ans = 0;
    int ret = ft_recursive_power(nb, power);
    if (ret != ans) {
      D_FAILED("Wrong with input [%d^%d]:\n" RED "    Your:     [%d]\n" GREEN "    Correct:  [%d]", nb, power, ret, ans);
    }
    }, {});
CREATE_TEST("5^0", {
    int nb = 5;
    int power = 0;
    int ans = 1;
    int ret = ft_recursive_power(nb, power);
    if (ret != ans) {
      D_FAILED("Wrong with input [%d^%d]:\n" RED "    Your:     [%d]\n" GREEN "    Correct:  [%d]", nb, power, ret, ans);
    }
    }, {});
CREATE_TEST("10^6", {
    int nb = 10;
    int power = 6;
    int ans = 1000000;
    int ret = ft_recursive_power(nb, power);
    if (ret != ans) {
      D_FAILED("Wrong with input [%d^%d]:\n" RED "    Your:     [%d]\n" GREEN "    Correct:  [%d]", nb, power, ret, ans);
    }
    }, {});
CREATE_TEST("-2^31", {
    int nb = -2;
    int power = 31;
    int ans = -2147483648;
    int ret = ft_recursive_power(nb, power);
    if (ret != ans) {
      D_FAILED("Wrong with input [%d^%d]:\n" RED "    Your:     [%d]\n" GREEN "    Correct:  [%d]", nb, power, ret, ans);
    }
    }, {});
CREATE_TEST("-2^-5", {
    int nb = -2;
    int power = -5;
    int ans = 0;
    int ret = ft_recursive_power(nb, power);
    if (ret != ans) {
      D_FAILED("Wrong with input [%d^%d]:\n" RED "    Your:     [%d]\n" GREEN "    Correct:  [%d]", nb, power, ret, ans);
    }
    }, {});

CREATE_EX(
    &test_0,
    &test_1,
    &test_2,
    &test_3,
    &test_4,
    &test_5,
    &test_6,
    &test_7,
    )
