#include "core_utils.h"

int ft_fibonacci(int nb);

CREATE_TEST("Info", {}, { MISC("    " FN_NAME " MUST CALL " FN_NAME"."); MISC("    " FN_NAME " is the only fonction of C05 that could time out"); });
CREATE_TEST("0", {
    int nb = 0;
    int ans = 0;
    int ret = ft_fibonacci(nb);
    if (ret != ans) {
      D_FAILED("Wrong with input [%d]:\n" RED "    Your:     [%d]\n" GREEN "    Correct:  [%d]", nb, ret, ans);
    }
    }, {});
CREATE_TEST("1", {
    int nb = 1;
    int ans = 1;
    int ret = ft_fibonacci(nb);
    if (ret != ans) {
      D_FAILED("Wrong with input [%d]:\n" RED "    Your:     [%d]\n" GREEN "    Correct:  [%d]", nb, ret, ans);
    }
    }, {});
CREATE_TEST("10", {
    int nb = 10;
    int ans = 55;
    int ret = ft_fibonacci(nb);
    if (ret != ans) {
      D_FAILED("Wrong with input [%d]:\n" RED "    Your:     [%d]\n" GREEN "    Correct:  [%d]", nb, ret, ans);
    }
    }, {});
CREATE_TEST("-1", {
    int nb = -1;
    int ans = -1;
    int ret = ft_fibonacci(nb);
    if (ret != ans) {
      D_FAILED("Wrong with input [%d]:\n" RED "    Your:     [%d]\n" GREEN "    Correct:  [%d]", nb, ret, ans);
    }
    }, {});
CREATE_TEST("-5489", {
    int nb = -5489;
    int ans = -1;
    int ret = ft_fibonacci(nb);
    if (ret != ans) {
      D_FAILED("Wrong with input [%d]:\n" RED "    Your:     [%d]\n" GREEN "    Correct:  [%d]", nb, ret, ans);
    }
    }, {});
CREATE_TEST("45", {
    int nb = 45;
    int ans = 1134903170;
    int ret = ft_fibonacci(nb);
    if (ret != ans) {
      D_FAILED("Wrong with input [%d]:\n" RED "    Your:     [%d]\n" GREEN "    Correct:  [%d]", nb, ret, ans);
    }
    }, {});
CREATE_TEST("20", {
    int nb = 20;
    int ans = 6765;
    int ret = ft_fibonacci(nb);
    if (ret != ans) {
      D_FAILED("Wrong with input [%d]:\n" RED "    Your:     [%d]\n" GREEN "    Correct:  [%d]", nb, ret, ans);
    }
    }, {});
CREATE_TEST("46", {
    int nb = 46;
    int ans = 1836311903;
    int ret = ft_fibonacci(nb);
    if (ret != ans) {
      D_FAILED("Wrong with input [%d]:\n" RED "    Your:     [%d]\n" GREEN "    Correct:  [%d]", nb, ret, ans);
    }
    }, {});

CREATE_EX(
    &test_0,
    &test_1,
    &test_2,
    &test_3,
    &test_4,
    &test_5,
    &test_6,
    &test_7,
    &test_8,
    )

