#include "core_utils.h"

int ft_sqrt(int nb);

CREATE_TEST("0", {
    int nb = 0;
    int ans = 0;
    int ret = ft_sqrt(nb);
    if (ret != ans) {
      D_FAILED("Wrong with input [%d]:\n" RED "    Your:     [%d]\n" GREEN "    Correct:  [%d]", nb, ret, ans);
    }
    }, {});
CREATE_TEST("1", {
    int nb = 1;
    int ans = 1;
    int ret = ft_sqrt(nb);
    if (ret != ans) {
      D_FAILED("Wrong with input [%d]:\n" RED "    Your:     [%d]\n" GREEN "    Correct:  [%d]", nb, ret, ans);
    }
    }, {});
CREATE_TEST("4", {
    int nb = 4;
    int ans = 2;
    int ret = ft_sqrt(nb);
    if (ret != ans) {
      D_FAILED("Wrong with input [%d]:\n" RED "    Your:     [%d]\n" GREEN "    Correct:  [%d]", nb, ret, ans);
    }
    }, {});
CREATE_TEST("9", {
    int nb = 9;
    int ans = 3;
    int ret = ft_sqrt(nb);
    if (ret != ans) {
      D_FAILED("Wrong with input [%d]:\n" RED "    Your:     [%d]\n" GREEN "    Correct:  [%d]", nb, ret, ans);
    }
    }, {});
CREATE_TEST("-1", {
    int nb = -1;
    int ans = 0;
    int ret = ft_sqrt(nb);
    if (ret != ans) {
      D_FAILED("Wrong with input [%d]:\n" RED "    Your:     [%d]\n" GREEN "    Correct:  [%d]", nb, ret, ans);
    }
    }, {});
CREATE_TEST("-5489", {
    int nb = -5489;
    int ans = 0;
    int ret = ft_sqrt(nb);
    if (ret != ans) {
      D_FAILED("Wrong with input [%d]:\n" RED "    Your:     [%d]\n" GREEN "    Correct:  [%d]", nb, ret, ans);
    }
    }, {});
CREATE_TEST("2147395600", {
    int nb = 2147395600;
    int ans = 46340;
    int ret = ft_sqrt(nb);
    if (ret != ans) {
      D_FAILED("Wrong with input [%d]:\n" RED "    Your:     [%d]\n" GREEN "    Correct:  [%d]", nb, ret, ans);
    }
    }, {});
CREATE_TEST("2250001", {
    int nb = 2250001;
    int ans = 0;
    int ret = ft_sqrt(nb);
    if (ret != ans) {
      D_FAILED("Wrong with input [%d]:\n" RED "    Your:     [%d]\n" GREEN "    Correct:  [%d]", nb, ret, ans);
    }
    }, {});
CREATE_TEST("2147483647", {
    int nb = 2147483647;
    int ans = 0;
    int ret = ft_sqrt(nb);
    if (ret != ans) {
      D_FAILED("Wrong with input [%d]:\n" RED "    Your:     [%d]\n" GREEN "    Correct:  [%d]", nb, ret, ans);
    }
    }, {});

CREATE_EX(
    &test_0,
    &test_1,
    &test_2,
    &test_3,
    &test_4,
    &test_5,
    &test_6,
    &test_7,
    &test_8,
    )

