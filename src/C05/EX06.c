#include "core_utils.h"

int ft_is_prime(int nb);

CREATE_TEST("0", {
    int nb = 0;
    int ans = 0;
    int ret = ft_is_prime(nb);
    if (ret != ans) {
      D_FAILED("Wrong with input [%d]:\n" RED "    Your:     [%d]\n" GREEN "    Correct:  [%d]", nb, ret, ans);
    }
    }, {});
CREATE_TEST("1", {
    int nb = 1;
    int ans = 0;
    int ret = ft_is_prime(nb);
    if (ret != ans) {
      D_FAILED("Wrong with input [%d]:\n" RED "    Your:     [%d]\n" GREEN "    Correct:  [%d]", nb, ret, ans);
    }
    }, {});
CREATE_TEST("13", {
    int nb = 13;
    int ans = 1;
    int ret = ft_is_prime(nb);
    if (ret != ans) {
      D_FAILED("Wrong with input [%d]:\n" RED "    Your:     [%d]\n" GREEN "    Correct:  [%d]", nb, ret, ans);
    }
    }, {});
CREATE_TEST("458", {
    int nb = 458;
    int ans = 0;
    int ret = ft_is_prime(nb);
    if (ret != ans) {
      D_FAILED("Wrong with input [%d]:\n" RED "    Your:     [%d]\n" GREEN "    Correct:  [%d]", nb, ret, ans);
    }
    }, {});
CREATE_TEST("214745911", {
    int nb = 214745911;
    int ans = 1;
    int ret = ft_is_prime(nb);
    if (ret != ans) {
      D_FAILED("Wrong with input [%d]:\n" RED "    Your:     [%d]\n" GREEN "    Correct:  [%d]", nb, ret, ans);
    }
    }, {});
CREATE_TEST("-5489", {
    int nb = -5489;
    int ans = 0;
    int ret = ft_is_prime(nb);
    if (ret != ans) {
      D_FAILED("Wrong with input [%d]:\n" RED "    Your:     [%d]\n" GREEN "    Correct:  [%d]", nb, ret, ans);
    }
    }, {});
CREATE_TEST("369", {
    int nb = 369;
    int ans = 0;
    int ret = ft_is_prime(nb);
    if (ret != ans) {
      D_FAILED("Wrong with input [%d]:\n" RED "    Your:     [%d]\n" GREEN "    Correct:  [%d]", nb, ret, ans);
    }
    }, {});
CREATE_TEST("2147483647", {
    int nb = 2147483647;
    int ans = 1;
    int ret = ft_is_prime(nb);
    if (ret != ans) {
      D_FAILED("Wrong with input [%d]:\n" RED "    Your:     [%d]\n" GREEN "    Correct:  [%d]", nb, ret, ans);
    }
    }, {});
CREATE_TEST("25", {
    int nb = 25;
    int ans = 0;
    int ret = ft_is_prime(nb);
    if (ret != ans) {
      D_FAILED("Wrong with input [%d]:\n" RED "    Your:     [%d]\n" GREEN "    Correct:  [%d]", nb, ret, ans);
    }
    }, {});

CREATE_EX(
    &test_0,
    &test_1,
    &test_2,
    &test_3,
    &test_4,
    &test_5,
    &test_6,
    &test_7,
    &test_8,
    )

