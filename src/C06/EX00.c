#include "core_utils.h"

int main();

extern const char *environ[];

CREATE_TEST("", {
    return main(argc, argv, environ);
    }, {
    const int len = strlen(argv[0]) + 1;
    char str[len + 1];
    memcpy(str, argv[0], len);
    str[len - 1] = '\n';
    str[len] = '\0';
    return wescande_memcmp_fd_no_print(fd, str, len);
    });

CREATE_EX(
    &test_0,
    )
