#include "core_utils.h"

int main();

extern const char *environ[];

int cstring_cmp(const void *a, const void *b) 
{
    const char **ia = (const char **)a;
    const char **ib = (const char **)b;
    return strcmp(*ia, *ib);
}

CREATE_TEST("", {
    return main(argc, argv, environ);
    }, {
    qsort(argv + 1, argc - 1, sizeof(char *), cstring_cmp);
    int len = 0;
    for (int i = 1; i < argc; ++i) {
      len += strlen(argv[i]) + 1;
    }
    char str[len + 1];
    str[0] = 0;
    for (int i = 1; i < argc; ++i) {
      strcat(str, argv[i]);
      strcat(str, "\n");
    }
    return wescande_memcmp_fd_no_print(fd, str, len);
    });

CREATE_EX(
    &test_0,
    )



