#include "core_utils.h"
#include "c01_utils.h"

char *ft_strdup(char *str);

CREATE_TEST("Is malloc protected ?", {
    force_alloc_to_fail = 1;
    void * ret = ft_strdup("");
    force_alloc_to_fail = 0;
    if (ret != NULL) {
      D_FAILED("Malloc must be protected");
    }
    }, { });
CREATE_TEST("", {
    char ref[] = "";
    char *res = ft_strdup(ref);
    if (res == NULL) {
      D_FAILED("Call " FN_NAME "(%s): should not return NULL", ref);
      return 0;
    }
    if (strcmp(res, ref)) {
      D_FAILED("Wrong dup:\n" RED "    Your:     [%s]\n" GREEN "    Correct:  [%s]", res, ref);
    }
    free(res);
    }, { });
CREATE_TEST("", {
    char ref[] = "simple chaine";
    char *res = ft_strdup(ref);
    if (res == NULL) {
      D_FAILED("Call " FN_NAME "(%s): should not return NULL", ref);
      return 0;
    }
    if (strcmp(res, ref)) {
      D_FAILED("Wrong dup:\n" RED "    Your:     [%s]\n" GREEN "    Correct:  [%s]", res, ref);
    }
    free(res);
    }, { });
CREATE_TEST("", {
    char ref[] = STR_EX_05;
    char *res = ft_strdup(ref);
    if (res == NULL) {
      D_FAILED("Call " FN_NAME "(%s): should not return NULL", ref);
      return 0;
    }
    if (strcmp(res, ref)) {
      D_FAILED("Wrong dup:\n" RED "    Your:     [%s]\n" GREEN "    Correct:  [%s]", res, ref);
    }
    free(res);
    }, { });

CREATE_EX(
    &test_0,
    &test_1,
    &test_2,
    &test_3,
    )




