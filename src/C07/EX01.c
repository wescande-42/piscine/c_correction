#include "core_utils.h"
#include "c01_utils.h"

int *ft_range(int min, int max);

CREATE_TEST("Is malloc protected ?", {
    force_alloc_to_fail = 1;
    void * ret = ft_range(0, 5);
    force_alloc_to_fail = 0;
    if (ret != NULL) {
      D_FAILED("Malloc must be protected");
      return false;
    }
    }, { });
CREATE_TEST("min == max", {
    const int min = 0;
    const int max = 0;
    int *tab = ft_range(min, max);
    if (tab != NULL) {
      D_FAILED("Call " FN_NAME "(%d, %d): should return NULL", min, max);
      return 0;
    }
    }, { });
CREATE_TEST("min > max", {
    const int min = 50;
    const int max = 0;
    int *tab = ft_range(min, max);
    if (tab != NULL) {
      D_FAILED("Call " FN_NAME "(%d, %d): should return NULL", min, max);
      return 0;
    }
    }, { });
CREATE_TEST("normal", {
    const int min = 50;
    const int max = 5000;
    int *tab = ft_range(min, max);
    for (int i = 0; i < max - min; ++i) {
      if (tab[i] != min + i) {
        D_FAILED("Call " FN_NAME "(%d, %d): \n" RED "    Your:     `tab[%d]=%d`\n" GREEN "    Correct:  `tab[%d]=%d`", min, max, i, tab[i], i, min + i);
        free(tab);
        return 0;
      }
    }
    free(tab);
    }, { });

CREATE_EX(
    &test_0,
    &test_1,
    &test_2,
    &test_3,
    )





