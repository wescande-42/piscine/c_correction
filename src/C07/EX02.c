#include "core_utils.h"
#include "c01_utils.h"

int ft_ultimate_range(int **range, int min, int max);

CREATE_TEST("Is malloc protected ?", {
    const int min = 0;
    const int max = 50;
    int *r=(int*)8745;
    force_alloc_to_fail = 1;
    int ret = ft_ultimate_range(&r, min, max);
    force_alloc_to_fail = 0;
    if (ret != -1) {
      D_FAILED("Call " FN_NAME "(int **range, %d, %d) with malloc desactivate: should return -1 not %d", min, max, ret);
      return 0;
    }
    }, { });
CREATE_TEST("min == max", {
    const int min = 0;
    const int max = 0;
    int *r=(int*)8745;
    int ret = ft_ultimate_range(&r, min, max);
    if (ret != 0) {
      D_FAILED("Call " FN_NAME "(int **range, %d, %d): should return 0 not %d", min, max, ret);
      return 0;
    }
    if (r != NULL) {
      D_FAILED("Call " FN_NAME "(int **range, %d, %d): range should be NULL instead of %p", min, max, r);
      return 0;
    }
    }, { });
CREATE_TEST("min > max", {
    const int min = 50;
    const int max = 0;
    int *r=(int*)8745;
    int ret = ft_ultimate_range(&r, min, max);
    if (ret != 0) {
      D_FAILED("Call " FN_NAME "(int **range, %d, %d): should return 0 not %d", min, max, ret);
      return 0;
    }
    if (r != NULL) {
      D_FAILED("Call " FN_NAME "(int **range, %d, %d): range should be NULL instead of %p", min, max, r);
      return 0;
    }
    }, { });
CREATE_TEST("normal", {
    const int min = 50;
    const int max = 500;
    int *r=(int*)8745;
    int ret = ft_ultimate_range(&r, min, max);
    if (ret != max - min) {
      D_FAILED("Call " FN_NAME "(int **range, %d, %d): should return %d not %d", min, max, max - min, ret);
      return 0;
    }
    for (int i = 0; i < max - min; ++i) {
      if (r[i] != min + i) {
        D_FAILED("Call " FN_NAME "(%d, %d): \n" RED "    Your:     `range[%d]=%d`\n" GREEN "    Correct:  `range[%d]=%d`", min, max, i, r[i], i, min + i);
        free(r);
        return 0;
      }
    }
    free(r);
    }, { });

CREATE_EX(
    &test_0,
    &test_1,
    &test_2,
    &test_3,
    )
