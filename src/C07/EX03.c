#include "core_utils.h"
#include "c01_utils.h"

char *ft_strjoin(int size, const char **strs, char *sep);

CREATE_TEST("Is malloc protected ?", {
    force_alloc_to_fail = 1;
    void * ret = ft_strjoin(0, NULL, NULL);
    force_alloc_to_fail = 0;
    if (ret != NULL) {
      D_FAILED("Malloc must be protected");
      return false;
    }
    }, { });
CREATE_TEST("size is 0", {
    char *str = ft_strjoin(0, NULL, NULL);
    if (str == NULL) {
      D_FAILED("call " FN_NAME "(0, NULL, NULL): must not return NULL");
      return 0;
    }
    if (strlen(str) != 0) {
      D_FAILED("call " FN_NAME "(0, NULL, NULL): strlen should be 0");
      return 0;
    }
    free(str);
    }, { });
CREATE_TEST("all argv", {
    char *str = ft_strjoin(1, argv, "Hey");
    if (str == NULL) {
      D_FAILED("call " FN_NAME "(0, NULL, NULL): must not return NULL");
      return 0;
    }
    write(1, str, strlen(str));
    free(str);
    }, { return wescande_memcmp_fd(fd, argv[0], strlen(argv[0])); });
CREATE_TEST("one str", {
    char *sep = "| 4-2 sep |";
    char *str = ft_strjoin(argc, argv, sep);
    if (str == NULL) {
      D_FAILED("call " FN_NAME "(0, NULL, NULL): must not return NULL");
      return 0;
    }
    int i = 0;
    char *s = str;
    const int sep_len = strlen(sep);
    while (*str && i < argc) {
      const int argLen = strlen(argv[i]);
      if (strncmp(str, argv[i], argLen)) {
        D_FAILED("One word missing\n" RED "    Your:     `[%s]`\n" GREEN "    Correct:  `[%s]...`", s, argv[i]);
        return 0;
      }
      str += argLen;
      if (i < argc - 1) {
      if (strncmp(str, sep, sep_len)) {
        D_FAILED("One sep missing\n" RED "    Your:     `[%s]`\n" GREEN "    Correct:  `[%s]...`", s, sep);
        return 0;
      }
      str += sep_len;
      }
      ++i;
    }
    if (*str) {
      D_FAILED("Too many word in output: [%s]", str);
    }
    free(s);
    }, {});

CREATE_EX(
    &test_0,
    &test_1,
    &test_2,
    &test_3,
    )

