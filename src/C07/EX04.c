#include "core_utils.h"

char *ft_convert_base(char *n, char *bfrom, char *bto);

CREATE_TEST("Is malloc protected ?", {
    force_alloc_to_fail = 1;
    void * ret = ft_convert_base("coucou", "cou", "cou");
    force_alloc_to_fail = 0;
    if (ret != NULL) {
      D_FAILED("Malloc must be protected");
      return false;
    }
    }, { });
CREATE_TEST("from base empty", {
    wescande_output_str(ft_convert_base("01", "", "01"));
    }, {
    wescande_memcmp_fd(fd, "NULL", 4);
    });
CREATE_TEST("from base taille 1", {
    wescande_output_str(ft_convert_base("01", "0", "01"));
    }, {
    wescande_memcmp_fd(fd, "NULL", 4);
    });
CREATE_TEST("from base same char", {
    wescande_output_str(ft_convert_base("01", "01234526789", "01"));
    }, {
    wescande_memcmp_fd(fd, "NULL", 4);
    });
CREATE_TEST("from base with '+' char", {
    wescande_output_str(ft_convert_base("01", "01234+56789", "01"));
    }, {
    wescande_memcmp_fd(fd, "NULL", 4);
    });
CREATE_TEST("from base with '-' char", {
    wescande_output_str(ft_convert_base("01", "0123456789-", "01"));
    }, {
    wescande_memcmp_fd(fd, "NULL", 4);
    });
CREATE_TEST("to base empty", {
    wescande_output_str(ft_convert_base("01", "01", ""));
    }, {
    wescande_memcmp_fd(fd, "NULL", 4);
    });
CREATE_TEST("to base taille 1", {
    wescande_output_str(ft_convert_base("01", "01", "0"));
    }, {
    wescande_memcmp_fd(fd, "NULL", 4);
    });
CREATE_TEST("to base same char", {
    wescande_output_str(ft_convert_base("01", "01", "01234526789"));
    }, {
    wescande_memcmp_fd(fd, "NULL", 4);
    });
CREATE_TEST("to base with '+' char", {
    wescande_output_str(ft_convert_base("01", "01", "01234+56789"));
    }, {
    wescande_memcmp_fd(fd, "NULL", 4);
    });
CREATE_TEST("to base with '-' char", {
    wescande_output_str(ft_convert_base("01", "01", "0123456789-"));
    }, {
    wescande_memcmp_fd(fd, "NULL", 4);
    });
CREATE_TEST("42 in 01 to 01", {
    char *nbr     = "42";
    char *bfrom   = "01";
    char *bto     = "01";
    char * ret = ft_convert_base(nbr, bfrom, bto);
    wescande_output_str(ret);
    free(ret);
    }, { return wescande_memcmp_fd(fd, "0", 1); } );
CREATE_TEST("-42 in 01234 to 01", {
    char *nbr     = "  \t \n   -+----42";
    char *bfrom   = "01234";
    char *bto     = "01";
    char * ret = ft_convert_base(nbr, bfrom, bto);
    wescande_output_str(ret);
    free(ret);
    }, { return wescande_memcmp_fd(fd, "-10110", 6); } );
CREATE_TEST("-FFEFF42 in hexa to mickey", {
    char *nbr     = "  \t \n   -000FFEFF42schoolofCode";
    char *bfrom   = "0123456789ABCDEF";
    char *bto     = "mickey";
    char * ret = ft_convert_base(nbr, bfrom, bto);
    wescande_output_str(ret);
    free(ret);
    }, { return wescande_memcmp_fd(fd, "-eckeemkccec", 12); } );
CREATE_TEST("-F in hexa to mickey", {
    char *nbr     = "  \t \n   -000FschoolofCode";
    char *bfrom   = "0123456789ABCDEF";
    char *bto     = "mickey";
    char * ret = ft_convert_base(nbr, bfrom, bto);
    wescande_output_str(ret);
    free(ret);
    }, { return wescande_memcmp_fd(fd, "-ck", 3); } );
CREATE_TEST("INT_MIN in dec to octal", {
    char *nbr     = "  \t \n   -2147483648choolofCode";
    char *bfrom   = "0123456789";
    char *bto     = "01234567";
    char * ret = ft_convert_base(nbr, bfrom, bto);
    wescande_output_str(ret);
    free(ret);
    }, { return wescande_memcmp_fd(fd, "-20000000000", 12); } );
CREATE_TEST("INT_MAX in bulshitBASE to grandMotheR$%", {
    char *nbr     = "  \t \n \r  EblllblAuTadksachoolofCode";
    char *bfrom   = "bulshitBASE";
    char *bto     = "grandMotheR$%";
    char * ret = ft_convert_base(nbr, bfrom, bto);
    wescande_output_str(ret);
    free(ret);
    }, { return wescande_memcmp_fd(fd, "aha$RdRRR", 9); } );

CREATE_TEST("INT_MAX in 0123456789 to binary$%", {
    char *nbr     = "2147483647";
    char *bfrom   = "0123456789";
    char *bto     = "01";
    char * ret = ft_convert_base(nbr, bfrom, bto);
    wescande_output_str(ret);
    free(ret);
    }, { return wescande_memcmp_fd(fd, "1111111111111111111111111111111", 31); } );

CREATE_TEST("INT_MIN in 0123456789 to binary$%", {
    char *nbr     = "-2147483648";
    char *bfrom   = "0123456789";
    char *bto     = "01";
    char * ret = ft_convert_base(nbr, bfrom, bto);
    wescande_output_str(ret);
    free(ret);
    }, { return wescande_memcmp_fd(fd, "-10000000000000000000000000000000", 33); } );

CREATE_EX(
    &test_0,
    &test_1,
    &test_2,
    &test_3,
    &test_4,
    &test_5,
    &test_6,
    &test_7,
    &test_8,
    &test_9,
    &test_10,
    &test_11,
    &test_12,
    &test_13,
    &test_14,
    &test_15,
    &test_16,
    &test_17,
    &test_18,
    )

