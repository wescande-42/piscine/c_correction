#include "core_utils.h"

char **ft_split(const char *str, const char *charset);


CREATE_TEST("Is malloc protected ?", {
    force_alloc_to_fail = 1;
    void * ret = ft_split("coucoucoucoucoucoucoucoucou", "c");
    force_alloc_to_fail = 0;
    if (ret != NULL) {
      D_FAILED("Malloc must be protected");
      return false;
    }
    _exit(0);
    }, { });
CREATE_TEST("Is malloc protected ?", {
    force_alloc_to_fail = 2;
    void * ret = ft_split("coucoucoucoucoucoucoucoucou", "c");
    force_alloc_to_fail = 0;
    if (ret != NULL) {
      D_FAILED("Malloc must be protected");
      return false;
    }
    _exit(0);
    }, { });
CREATE_TEST("Is malloc protected ?", {
    force_alloc_to_fail = 3;
    void * ret = ft_split("coucoucoucoucoucoucoucoucou", "c");
    force_alloc_to_fail = 0;
    if (ret != NULL) {
      D_FAILED("Malloc must be protected");
      return false;
    }
    _exit(0);
    }, { });
void ft_strcpy(char *dest, char *src) {
  while (*src) {
    *dest++ = *src++;
  }
  *dest++ = *src++;
}

void check_split(const char *str, const char *charset) {
  char **result = ft_split(str, charset);
  char **r = result;
  char *dup = strdup(str);
  char *tok = strtok(dup, charset);
  while (*r && tok) {
    if (strcmp(tok, *r)) {
      TO_PRINTABLE(t, tok, strlen(tok) + 1);
      TO_PRINTABLE(res, *r, strlen(*r) + 1);
      D_FAILED("Wrong result: at %ld\n" RED "  Yours:     [%s]\n" GREEN "  Correct:   [%s]\n", r - result, res, t);
    }
    tok = strtok(NULL, charset);
    free(*r++);
  }
  if (tok) {
      TO_PRINTABLE(t, tok, strlen(tok) + 1);
      D_FAILED("Expect " GREEN "[%s]", t);
  }
  while (*r) {
      TO_PRINTABLE(res, *r, strlen(*r) + 1);
      D_FAILED("Got unexpect str [%s]\t at %ld", res, r - result);
    free(*r++);
  }
  free(result);
  free(dup);
}
static const char *str0="Hey \t from a \t normal str      please return     me   ";
static const char *charset0 = " \t";
CREATE_TEST("", { check_split(str0, charset0); }, { });

static const char *str1="";
static const char *charset1 = "";
CREATE_TEST("", { check_split(str1, charset1); }, { });

static const char *str2="WORD";
static const char *charset2 = "";
CREATE_TEST("", { check_split(str2, charset2); }, { });

static const char *str3=" AAAAAWORDAAAAaaaa 1498\n32 23 3421 32108 21021 8849384 121 823821 8";
static const char *charset3 = " A 3   \n ";
CREATE_TEST("", { check_split(str3, charset3); }, { });

CREATE_EX(
    &test_0,
    &test_1,
    &test_2,
    &test_3,
    &test_4,
    &test_5,
    &test_6,
    )

