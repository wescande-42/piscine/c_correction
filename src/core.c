#include <stdio.h>
#include <time.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <errno.h>
#include <fcntl.h>
#include <string.h>
#include <signal.h>
#include <ctype.h>
#include <dlfcn.h>
#include <unistd.h>

#include "core_utils.h"

static uint64_t wescande_time_ms() {
  struct timespec t;
  clock_gettime(CLOCK_MONOTONIC, &t);
  return t.tv_sec * 1000 + t.tv_nsec / (1000 * 1000);
}

// int msleep(long msec)
// {
//     struct timespec ts;
//     int res;

//     ts.tv_sec = msec / 1000;
//     ts.tv_nsec = (msec % 1000) * 1000000;

//     do {
//         res = nanosleep(&ts, &ts);
//     } while (res && errno == EINTR);
//     return res;
// }

static int wescande_waitpid_timed(pid_t pid, int *wstatus, const uint64_t timeout_ms) {
  const uint64_t start_ms = wescande_time_ms();
  do {
    int option = WUNTRACED;
#ifndef NOTIMEOUT
    option |= WNOHANG;
#endif
    const pid_t w = waitpid(pid, wstatus, option);
    if (w == -1) {
      return -EINVAL;
    } else if (w) {
      return w;
    }
  } while (wescande_time_ms() < start_ms + timeout_ms);
  kill(pid, SIGKILL);
  return -ETIME;
}

static int wescande_fork_wait_check(int argc, const char *argv[], const int fd[4], test_handler_t *test) {
  int ret = 1;
  const uint64_t start_ms = wescande_time_ms();
  pid_t pid;

  if (0 > (pid = fork())) {
    INTERN_ERR("Fail to fork");
    return ret;
  }
  if (pid == 0) {
    dup2(fd[1], 1);
    dup2(fd[3], 2);
    exit(test->testfn(argc, argv, test));
  }
  const int flags = fcntl(fd[0], F_GETFL, 0);
  fcntl(fd[0], F_SETFL, flags | O_NONBLOCK);
  const int flags2 = fcntl(fd[2], F_GETFL, 0);
  fcntl(fd[2], F_SETFL, flags2 | O_NONBLOCK);
  int wstatus;
  uint64_t timeout_ms = test->timeout_ms;
#ifdef EASYTIME
  timeout_ms *= 25;
#endif
  const int wresult = wescande_waitpid_timed(pid, &wstatus, timeout_ms);
  switch (wresult) {
    case -EINVAL:
      INTERN_ERR("failed to wait child");
      break;
    case -ETIME:
#ifdef EASYTIME
      FAILED("ERR: Timeout: delay was %lu ms.\n  Use -Z to remove timeout. " UNDERLINE "/!\\" EOF_UNDERLINE " infinite loop are bad for this option.", (unsigned long)timeout_ms);
#else
      FAILED("ERR: Timeout: delay was %lu ms. Use -t to multiply limite by 25.\n  Use -Z to remove timeout. " UNDERLINE "/!\\" EOF_UNDERLINE " infinite loop are bad for this option.", (unsigned long)timeout_ms);
#endif
      ret = -ETIME;
      break;
    default:
      if (WIFEXITED(wstatus)) {
        const int exit_status = WEXITSTATUS(wstatus);
        if (exit_status == 0) {
          if (test->checkfn(argc, argv, fd[0], fd[2], test)) {
            const unsigned long delta_ms = (unsigned long)(wescande_time_ms() - start_ms);
            if (delta_ms > timeout_ms / 5) {
              SUCCESS("OK  =>" RED " But your function is REALLY too slow it took %lu ms. That's huge!", delta_ms);
            } else if (delta_ms > timeout_ms / 30) {
              SUCCESS("OK  =>" YELLOW " Your function could be better. It took %lu ms", delta_ms);
            } else {
              SUCCESS("OK (%lu ms)", delta_ms);
            }
            return 0;
          }
          // FAILED("ERR: Test failed");
        } else {
          FAILED("ERR: Process ended with status %d", exit_status);
        }
      } else if (WIFSIGNALED(wstatus)) {
        FAILED("ERR: Process killed by signal %d", WTERMSIG(wstatus));
      } else if (WIFSTOPPED(wstatus)) {
        FAILED("ERR: Process stopped by signal %d", WSTOPSIG(wstatus));
      }
  };
  wescande_check_empty_stderr(fd[2]);
  wescande_check_empty_stdout(fd[0]);
  return ret;
}

#define BUF_SIZE (1000 * 1000)
static char buffer[BUF_SIZE];

// bool canprint(char c) {
//   return c >= ' ' && c <= '~';
// }

int wescande_convert_to_printable(const bool pad, char *dest, const size_t capacity, const char *src, const ssize_t len) {
  ssize_t convert_space = capacity - len - sizeof(UNDERLINE) - sizeof(EOF_UNDERLINE) - 3;
  bool isunderlined = true;
  const char *base = "0123456789abcdefg";
  char *cursor = dest;
  for (ssize_t i = 0; i < len; ++i) {
    if (isprint(src[i]) != 0 || convert_space < 0) {
      if (!isunderlined) {
        isunderlined = true;
        memcpy(cursor, EOF_UNDERLINE, sizeof(EOF_UNDERLINE) - 1);
        cursor += sizeof(EOF_UNDERLINE) - 1;
        convert_space -= (sizeof(EOF_UNDERLINE) - 1);
      }
      *cursor++ = src[i];
    } else {
      if (isunderlined && !pad) {
        isunderlined = false;
        memcpy(cursor, UNDERLINE, sizeof(UNDERLINE) - 1);
        cursor += sizeof(UNDERLINE) - 1;
        convert_space -= (sizeof(UNDERLINE) - 1);
      }
      unsigned char c = src[i];
      *cursor++ = '\\';
      const int digit = c / 16;
      if (pad || digit != 0) {
        *cursor++ = base[digit];
        --convert_space;
      }
      *cursor++ = base[c % 16];
      convert_space -= 2;
    }
  }
  if (!isunderlined) {
    memcpy(cursor, EOF_UNDERLINE, sizeof(EOF_UNDERLINE) - 1 );
    cursor += sizeof(EOF_UNDERLINE) - 1;
  }
  *cursor = 0;
  return cursor - dest;
}

void wescande_output_str(const char *s) {
  if (!s) {
    return wescande_output_str("NULL");
  }
  write(1, s, strlen(s));
}
void wescande_output_num(int n) {
  char buf[20];
  char *cur = buf;
  int64_t N = (int64_t) n;
  if (N < 0) {
    *cur++ = '-';
    N *= -1;
  }
  int div = 1;
  while (N / div >= 10) {
    div *= 10;
  }
  while (div != 0) {
    *cur++ = '0' + N / div;
    N %= div;
    div /= 10;
  }
  write(1, buf, cur - buf);
}

static bool wescande_common_memcmp_fd(const int fd, const char *ref, ssize_t ref_len, bool print) {
  const ssize_t len = read(fd, buffer, BUF_SIZE);
  if (len == -1) {
    const int err = errno;
    if (err == EAGAIN) {
      if (ref_len == 0) { return true; }
      FAILED("Nothing to read, but expect %zd char", ref_len);
      return false;
    }
    INTERN_ERR("read failed: %s", strerror(err));
    return false;
  }
  buffer[len] = 0;
  TO_PRINTABLE(newBuf, buffer, len);
  TO_PRINTABLE(refBuf, ref, ref_len);
  if (!print) {
    newBuf = buffer;
    refBuf = ref;
  }
  if (ref_len != len) {
    ERR("Wrong length:\n" RED "  Your number of char:     [%zd]\n" GREEN "  Correct number of char:  [%zd]\n", len, ref_len);
    ERR("FYI:\n" RED "  Your output:     [%s]\n" GREEN "  Correct output:  [%s]", newBuf, refBuf);
    return false;
  }
  if (memcmp(buffer, ref, ref_len)) {
    ERR("Wrong output:\n" RED "  Your output:     [%s]\n" GREEN "  Correct output:  [%s]", newBuf, refBuf);
    return false;
  }
  return true;
}

bool wescande_memcmp_fd_no_print(const int fd, const char *ref, ssize_t ref_len) {
  return wescande_common_memcmp_fd(fd, ref, ref_len, false);
}

bool wescande_memcmp_fd(const int fd, const char *ref, ssize_t ref_len) {
  return wescande_common_memcmp_fd(fd, ref, ref_len, true);
}

ssize_t wescande_catch(const int fd, char **s) {
  *s = NULL;
  const ssize_t len = read(fd, buffer, BUF_SIZE);
  if (len == -1) {
    const int err = errno;
    if (err == EAGAIN) {
      return 0;
    }
    INTERN_ERR("read failed: %s", strerror(err));
    return 0;
  }
  buffer[len] = 0;
  *s = buffer;
  return len;
}

ssize_t wescande_check_empty_stderr(const int fd_err) {
  char *str_error;
  const ssize_t len_error = wescande_catch(fd_err, &str_error);
  if (!len_error) {
    return 0;
  }
  ERR("  Stderr:\n%s", str_error);
  return len_error;
}

ssize_t wescande_check_empty_stdout(const int fd) {
  char *str;
  const ssize_t len = wescande_catch(fd, &str);
  if (!len) {
    return 0;
  }
  str[len] = '\n';
  ERR("  Stdout:");
  write(1, str, len);
  TO_PRINTABLE(r, str, len);
  INFO("  Same data with non printable char displayed:\n" YELLOW "  {\n%s\n" YELLOW "  }", r);
  return len;
}

static int wescande_check_exercice(int argc, const char *argv[], const int fd[4], exercice_handler_t *exercice) {
  int result = 0;
  INFO("%s:", exercice->fn_name);
  for (int i = 0; exercice->tests[i] != NULL; ++i) {
    INFO("  " UNDERLINE "Test %d" EOF_UNDERLINE ": %s", i + 1, exercice->tests[i]->name);
    int ret = wescande_fork_wait_check(argc, argv, fd, exercice->tests[i]);
    if (ret && result <= 0) {
      result = ret;
    }
  }
  return result;
}

extern exercice_handler_t ex;

static int wescande_init(int fd[4]) {
  srand(time(NULL));
  if (-1 == pipe(fd)) {
    INTERN_ERR("Fail to pipe for stdout");
    return 1;
  }
  if (-1 == pipe(fd + 2)) {
    INTERN_ERR("Fail to pipe for stderr");
    return 1;
  }
  return 0;
}

int wescande_main(int argc, const char *argv[]) {
  int fd[4];
  if (wescande_init(fd)) {
    exit(1);
  }
  exit(wescande_check_exercice(argc, argv, fd, &ex));
}
#ifndef __APPLE__
#ifdef __x86_64__
asm(
    ".global _start\n"
    "_start:\n"
    "   xorl %ebp,%ebp\n"       // mark outermost stack frame
    "   movq 0(%rsp),%rdi\n"    // get argc
    "   lea 8(%rsp),%rsi\n"     // the arguments are pushed just below, so argv = %rbp + 8
    "   call wescande_main\n"   // call our custom main
    "   movq %rax,%rdi\n"       // take the main return code and use it as first argument for...
    "   movl $60,%eax\n"        // ... the exit syscall
    "   syscall\n"
    "   int3\n");               // just in case
#endif
#ifdef __i386__
asm(
    ".global _start\n"
    "_start:\n"
    "   xorl %ebp,%ebp\n"       // mark outermost stack frame
    "   movl 0(%esp),%edi\n"    // argc is on the top of the stack
    "   lea 4(%esp),%esi\n"     // as above, but with 4-byte pointers
    "   sub $8,%esp\n"          // the start starts 16-byte aligned, we have to push 2*4 bytes; "waste" 8 bytes
    "   pushl %esi\n"           // to keep it aligned after pushing our arguments
    "   pushl %edi\n"
    "   call wescande_main\n"   // call our custom main
    "   add $8,%esp\n"          // fix the stack after call (actually useless here)
    "   movl %eax,%ebx\n"       // take the main return code and use it as first argument for...
    "   movl $1,%eax\n"         // ... the exit syscall
    "   int $0x80\n"
    "   int3\n");               // just in case
#endif
#endif

static void* (*real_malloc)(size_t) = NULL;
int force_alloc_to_fail;

void m_init() {
  real_malloc = dlsym((void*)-1l, "malloc");
  if (real_malloc == NULL) {
    const char *str = RED "INTERNAL ERROR - please report to maintainer: FAILED TO BIND TO MALLOC" NO_COL "\n";
    wescande_output_str(str);
    exit(1);
  }
}

void *malloc(size_t size)
{
  if (force_alloc_to_fail > 1) {
    --force_alloc_to_fail;
  } else if (force_alloc_to_fail == 1)
    return NULL;
  if (real_malloc == NULL) {
    m_init();
  }
  return real_malloc(size);
}

