#!/usr/bin/env bash

set -e
tabs 4

#color
GREEN="\033[32m"
RED="\033[31m"
YELLOW="\033[33m"
BLUE="\033[34m"
#modifier
RAZ="\033[0m"
BOLD="\033[1m"
UNDERLINE="\033[4m"
EOF_UNDERLINE="\033[24m"

trap 'echo -e $BOLD$RED"SCRIPT FAILURE"$RAZ ' EXIT

SCRIPT_FOLDER=$(dirname $0)

info() {
  echo -e "$YELLOW"$@"$RAZ"
}
err() {
  echo -e "$BOLD$RED"$@"$RAZ"
}
err_exit() {
  err $@
  exit 1
}

usage() {
  ME="$UNDERLINE$(basename $0 | sed 's/\.sh$//')$EOF_UNDERLINE"
  echo -e \
$YELLOW"Usage:$RAZ
  $BOLD$ME [-h]$RAZ
  $BOLD$ME [-u]$RAZ
  $BOLD$ME [-qtZ] [-D day] [exercice..]$RAZ
    $BOLD-h:$RAZ  Display this message
    $BOLD-u:$RAZ  Update this script and exit
    $BOLD-D:$RAZ  Specify day to correct.
      -> Not needed if day folder name match '"$BOLD"[cC]00"$RAZ"'
    $BOLD-q:$RAZ  Hide advertise message
    $BOLD-t:$RAZ  Multiply timeout by 25
    $BOLD-Z:$RAZ  Disable timeout
  $RED|->FULL CORRECTION<--------------------------------------------$RAZ
  $RED| "$RAZ"Go in the day directory \`"$BOLD"cd [...]/C00"$RAZ"\`:
  $RED| "$RAZ"  $BOLD$GREEN$ME$RAZ
  $RED| "$RAZ"or if wrong folder name \`"$BOLD"cd [...]/foo"$RAZ"\`:
  $RED| "$RAZ"  $BOLD$GREEN$ME -D C01$RAZ
  $RED|->PARTIAL CORRECTION<------------------------------------------$RAZ
  $RED| "$RAZ"Go in the day directory \`"$BOLD"cd [...]/C00"$RAZ"\`:
  $RED| "$RAZ"  $BOLD$GREEN$ME ex08 ex05$RAZ
  $RED| "$RAZ"or directly from the ex directory \`"$BOLD"cd [...]/C04/ex08"$RAZ"\`:
  $RED| "$RAZ"  $BOLD$GREEN$ME$RAZ
  $RED| "$RAZ"or if double wrong folder name \`"$BOLD"cd [...]/foo/bar"$RAZ"\`:
  $RED| "$RAZ"  $BOLD$GREEN$ME -D C01 ex08$RAZ
  $RED| "$RAZ"or if wrong folder name \`"$BOLD"cd [...]/foo/ex08"$RAZ"\`:
  $RED| "$RAZ"  $BOLD$GREEN$ME -D C01$RAZ
  $RED----------------------------------------------------------------$RAZ
  "$YELLOW"Advanced option:$RAZ$BOLD [-f] [-F flags]..$RAZ
    $BOLD-f:$RAZ  Remove compile flags
    $BOLD-F:$RAZ  Add a flags to compile flags
"
  exit $@
}

advertise() {
  info "Non-printable character are converted to hexa and are underlined."
  info "  (E.g:"'\\n'" => 0x0a =>'"$UNDERLINE'\\a'$EOF_UNDERLINE"')\n"
  info "WARNING\n  "$BLUE"Please read the code and explain what is wrong.\n  "$BLUE"Also check if everything is understood, cheater will not be allowed.\n\n"
}

self_update(){
  (cd $SCRIPT_FOLDER && git pull)
  trap - EXIT
  exit $?
}

toUpper() {
  echo $@ | tr '[a-z]' '[A-Z]'
}

checkDay() {
  [[ "$@" =~ ^[cC][0-9][0-9]$ ]] || return 1
}
checkEx() {
  [[ "$@" =~ ^[eE][xX][0-9][0-9]$ ]] || return 1
}

update_WD() {
  tmp=$(echo $CWD_TMP | sed 's/^.*\///' )
  checkEx $tmp && CWD_TMP=$(echo $CWD_TMP | sed "s/\/$tmp//") && PWD_TMP=$(echo $PWD_TMP | sed "s/\/$tmp//") || true
}

getCurrentEx() {
  tmp=$(echo $CWD_TMP | sed 's/^.*\///' )
  checkEx $tmp && EX=$(toUpper $tmp) || {
    for dir in $(ls "$PWD"); do
      checkEx $dir && EX="$EX $(toUpper $dir)" || true
    done
  }
  update_WD
}

getCurrentDay() {
  tmp=$(echo $CWD_TMP | sed 's/^.*\///' )
  checkDay $tmp && DAY=$(toUpper $tmp) || err_exit "Dir $tmp: Not a valid name"
}

logDisplay() {
  if [[ "$VERBOSE" == "Y" ]]; then
    printf '%*s\n' "${COLUMNS:-$(tput cols)}" '' | tr ' ' -
    echo -e "    "$BOLD$BLUE"Output is:$RAZ"
    cat $LOG
    printf '%*s\n' "${COLUMNS:-$(tput cols)}" '' | tr ' ' -
    return 0
  else
    echo -e "    "$BOLD$RED"Log file is [$LOG]"$RAZ
    return 1
  fi
}

CWD=$(printf "%q\n" "$(pwd)")
CWD_TMP=$CWD
PWD_TMP=$PWD

OPTIND=1
VERBOSE="Y"
DAY=""
EX=""

USER_OPTIONS=''

[[ "$(uname)" == "Darwin" ]] &&
  {
    SYS_FLAGS="-nostartfiles -Wl,-e,_wescande_main" 
    ETIME=155
  } || {
    SYS_FLAGS="-nostartfiles -Wl,--no-as-needed"
    ETIME=194
  }

SYS_FLAGS="$SYS_FLAGS -ldl"
FLAGS="-ggdb -Wall -Wextra -Werror -Wshadow -fsanitize=address -fsanitize=undefined"

INCLUDE="-I $SCRIPT_FOLDER/include"

while getopts "hucD:E:fF:qtZ" opt; do
  case "$opt" in
    h)
      trap - EXIT
      usage 0
      ;;
    D)
      checkDay ${OPTARG} || err_exit "Day '${OPTARG}': Not a valid name"
      DAY="${OPTARG}"
      ;;
    E)
      checkEx ${OPTARG} || err_exit "Ex '${OPTARG}': Not a valid name"
      EX="${EX} $(echo ${OPTARG} | tr '[a-z]' '[A-Z]')"
      ;;
    f) FLAGS=""
      ;;
    F) FLAGS="${FLAGS} ${OPTARG}"
      ;;
    q)  VERBOSE="N"
      ;;
    u)  self_update
      ;;
    t)  USER_OPTIONS='-DEASYTIME'
      ;;
    Z)  USER_OPTIONS='-DNOTIMEOUT'
      ;;
    *) usage 1
      ;;
  esac
done

shift $((OPTIND-1))

for ex in "$@"
do
  checkEx $ex && EX="$EX $(toUpper $ex)" || err_exit "Not a valid exercice name: $ex"
done

source $SCRIPT_FOLDER/ex_list.sh

[[ "$EX" != "" ]] && update_WD || getCurrentEx
[[ "$DAY" != "" ]] || getCurrentDay
[[ "$DAY" == "" ]] && err_exit "Please specify day with -D option"
if [[ ! " ${DAY_LIST[@]} " =~ " ${DAY} " ]]; then
  err_exit "Day '$DAY' not available yet. You can only select one of '${DAY_LIST[@]}'"
fi
[[ "$EX" == "" ]] && err_exit "Please specify ex"


[[ "$VERBOSE" == "Y" ]] && advertise || true
[[ "$USER_OPTIONS" == "-DEASYTIME" ]] && err "Started with easy timeout option enabled"|| true
[[ "$USER_OPTIONS" == "-DNOTIMEOUT" ]] && err "Started with NO timeout option enabled"|| true

GENERIC_FILES="$SCRIPT_FOLDER/src/core.c"
COMPILE_CMD="gcc ${SYS_FLAGS} ${FLAGS} ${USER_OPTIONS} ${INCLUDE} ${GENERIC_FILES}"

CURRENT_DAY_EX=$DAY"_LIST"[@]

mkdir -p $SCRIPT_FOLDER/log

ret=0
echo -e $BLUE$DAY$RAZ:
for fullex in ${!CURRENT_DAY_EX}
do
  ex=$(toUpper $(echo "$fullex" | cut -c '-4'))
  name=$(echo "$fullex" | cut -c '6-' | sed 's/[^[:alnum:]_].*$//g')
  echo -en "  $YELLOW$ex$RAZ: $name "
  if [[ ! " ${EX[@]} " =~ " ${ex} " ]]; then
    echo -e $BOLD$YELLOW"-> SKIPED"$RAZ
    [[ $ret -eq 0 ]] && ret=-1
  else
    echo -en "\033[s"$BOLD$BLUE". . .Try to compil. . ."$RAZ"\033[u"
    user_file=$(eval echo $PWD_TMP"/"$fullex".c")
    wescande_file=$SCRIPT_FOLDER"/src/"$DAY"/"$ex".c"
    define="-DFN_NAME=\"$name\""
    BIN=$SCRIPT_FOLDER/$DAY.$ex.wescande.correction
    LOG=$SCRIPT_FOLDER/log/$DAY.$ex
    FULL_COMPILE_CMD="$COMPILE_CMD $define $user_file $wescande_file"
    $FULL_COMPILE_CMD -o $BIN 2>$LOG 1> $LOG || {
      echo -e "\033[K"$BOLD$RED"-> FAILED TO COMPIL "$UNDERLINE'/!\\'$RAZ
      echo -e $RED"Failed command is \n"$BOLD$RED$FULL_COMPILE_CMD
      logDisplay && true
      ret=1
      continue
    }
    echo -en "\033[K\033[s"$BOLD$BLUE"Compil with success. Running it now..."$RAZ"\033[u"
    $BIN $COMPILE_CMD 2>$LOG 1>$LOG || {
      bin_ret=$?
      [[ $bin_ret -eq $ETIME ]] && STR_ERR="TIME OUT" || STR_ERR="FAILED"
      echo -e "\033[K"$BOLD$RED"-> $STR_ERR"$RAZ
      logDisplay && echo -e $BOLD$RED$ex": [$name] => $STR_ERR"$RAZ
      [[ $ret -ne 1 ]] && ret=$bin_ret
      continue
    }
    echo -e "\033[K"$BOLD$GREEN"-> SUCCESS"$RAZ
  fi
done

rm -rf $SCRIPT_FOLDER/*.wescande.correction

case "$ret" in
  0) echo -e "\n"$BOLD$GREEN"$DAY: SUCCESS  (≧ ᗜ ≦)"$RAZ
    ;;
  1) echo -e "\n"$BOLD$RED"$DAY: FAILED"$RAZ
    ;;
  -1) echo -e "\n"$BOLD$YELLOW"$DAY: INCOMPLETE"$RAZ
    ;;
  $ETIME) echo -e "\n"$BOLD$RED"$DAY: TIMEOUT"$RAZ
    ;;
  *) echo -e "\n"$BOLD$RED"$DAY: FAILED"$RAZ
    ;;
esac

trap - EXIT
exit $ret
